package com.Thoopksa;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.Thoopksa.common.AppConstants;


public class MeasurementActivity extends AppCompatActivity {


    /*TextView tv_qalap_type_value,tv_qalap_length_value,tv_qalap_width_value,tv_qalap_h_v_value,tv_thoop_type_value,tv_chest_value,tv_waist_value,
            tv_pocket_type_value,tv_pocket_length_value,tv_pocket_width_value,tv_Shoulder_value,tv_sleves_type_value,
            tv_sleves_length_value,tv_sleves_width_value,tv_length_forward_value,tv_width_forward_value,tv_length_back_value,
            tv_banda_type_value,tv_banda_length_value,tv_banda_h_v_value;*/

    TextView tv_thoop_type_value, tv_qalap_type_value, tv_sleves_type_value, tv_banda_type_value, tv_banda_length_value, tv_header_title;


    EditText edt_qalap_width, edt_qalap_length, edt_chest, edt_waist, edt_pocket_type, edt_pocket_length_value, edt_pocket_width_value, edt_shoulder,
            edt_sleves_length_value, edt_sleves_width_value, edt_slaves_length_forward, edt_sleves_width_forward, edt_sleves_length_back;

    TextView lbl_thooptype, tv_thoop_type, lbl_qalaptype, tv_qalap_type, lbl_width, lbl_height, tv_chest, tv_waist, lbl_pockettype, tv_pocket_type, tv_pocket_length,
            tv_pocket_width, tv_shoulder, lbl_slevestype, tv_sleves_type, tv_length, tv_width, tv_length_forward, tv_width_forward, tv_length_back,
            lbl_bandatype, tv_banda_type, tv_banda_length;

    RadioButton rb_visible, rb_hidden, rb_banda_visible, rb_banda_hidden;

    ImageView img_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement);
        initView();
        setBaseWords();
        setData();

        if (AppConstants.orderList.getQalap_h_v().equals("1")) {
            rb_visible.setChecked(true);
            rb_hidden.setChecked(false);
        } else {
            rb_hidden.setChecked(true);
            rb_visible.setChecked(false);
        }
        if (AppConstants.orderList.getBanda_h_v().equals("1")) {
            rb_banda_visible.setChecked(true);
            rb_banda_hidden.setChecked(false);
        } else {
            rb_banda_hidden.setChecked(true);
            rb_banda_visible.setChecked(false);
        }
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setBaseWords() {

        lbl_thooptype.setText(AppConstants.baseWords.get("text_lbl_Thoop"));
        tv_thoop_type.setText(AppConstants.baseWords.get("thoop_type"));
        tv_thoop_type_value.setHint(AppConstants.baseWords.get("thoop_type"));
        lbl_qalaptype.setText(AppConstants.baseWords.get("Qalap"));
        tv_qalap_type.setText(AppConstants.baseWords.get("qalap_type"));
        tv_qalap_type_value.setHint(AppConstants.baseWords.get("qalap_type"));
        lbl_width.setText(AppConstants.baseWords.get("width"));
        lbl_height.setText(AppConstants.baseWords.get("height"));
        edt_qalap_width.setHint(AppConstants.baseWords.get("cm"));
        edt_qalap_length.setHint(AppConstants.baseWords.get("cm"));
        rb_visible.setText(AppConstants.baseWords.get("visible"));
        rb_hidden.setText(AppConstants.baseWords.get("hidden"));
        tv_chest.setText(AppConstants.baseWords.get("chest"));
        edt_chest.setHint(AppConstants.baseWords.get("inches"));
        edt_waist.setHint(AppConstants.baseWords.get("inches"));
        tv_waist.setText(AppConstants.baseWords.get("weist"));
        lbl_pockettype.setText(AppConstants.baseWords.get("pocket"));
        tv_pocket_type.setText(AppConstants.baseWords.get("pocket_type"));
        edt_pocket_type.setHint(AppConstants.baseWords.get("pocket_type"));
        tv_pocket_length.setText(AppConstants.baseWords.get("length"));
        edt_pocket_length_value.setHint(AppConstants.baseWords.get("inches"));
        edt_pocket_width_value.setHint(AppConstants.baseWords.get("inches"));
        tv_pocket_width.setText(AppConstants.baseWords.get("Width"));
        tv_shoulder.setText(AppConstants.baseWords.get("shoulder"));
        edt_shoulder.setHint(AppConstants.baseWords.get("rounded"));
        lbl_slevestype.setText(AppConstants.baseWords.get("sleves"));
        tv_sleves_type.setText(AppConstants.baseWords.get("sleves_type"));
        tv_sleves_type_value.setHint(AppConstants.baseWords.get("sleves_type"));
        tv_length.setText(AppConstants.baseWords.get("length"));
        edt_sleves_length_value.setHint(AppConstants.baseWords.get("cm"));
        tv_width.setText(AppConstants.baseWords.get("Width"));
        edt_sleves_width_value.setHint(AppConstants.baseWords.get("cm"));
        tv_length_forward.setText(AppConstants.baseWords.get("length_forward"));
        edt_slaves_length_forward.setHint(AppConstants.baseWords.get("cm"));
        tv_width_forward.setText(AppConstants.baseWords.get("width_forward"));
        edt_sleves_width_forward.setHint(AppConstants.baseWords.get("cm"));
        tv_length_back.setText(AppConstants.baseWords.get("length_back"));
        edt_sleves_length_back.setHint(AppConstants.baseWords.get("cm"));
        lbl_bandatype.setText(AppConstants.baseWords.get("banda"));
        tv_banda_type.setText(AppConstants.baseWords.get("banda_type"));
        tv_banda_type_value.setHint(AppConstants.baseWords.get("cm"));
        tv_banda_length.setText(AppConstants.baseWords.get("banda_length"));
        tv_banda_length_value.setHint(AppConstants.baseWords.get("cm"));
        rb_banda_visible.setText(AppConstants.baseWords.get("visible"));
        rb_banda_hidden.setText(AppConstants.baseWords.get("hidden"));
    }

    private void setData() {

        tv_qalap_type_value.setText(AppConstants.orderList.getQalap_type());
        edt_qalap_length.setText(AppConstants.orderList.getQalap_length());
        edt_qalap_width.setText(AppConstants.orderList.getQalap_width());
        tv_thoop_type_value.setText(AppConstants.orderList.getThoop_type());
        edt_chest.setText(AppConstants.orderList.getChest());
        edt_waist.setText(AppConstants.orderList.getWaist());
        edt_pocket_type.setText(AppConstants.orderList.getPocket_type());
        edt_pocket_length_value.setText(AppConstants.orderList.getPocket_length());
        edt_pocket_width_value.setText(AppConstants.orderList.getPocket_width());
        edt_shoulder.setText(AppConstants.orderList.getShoulder());
        tv_sleves_type_value.setText(AppConstants.orderList.getSleves_type());
        edt_sleves_length_value.setText(AppConstants.orderList.getSleves_length());
        edt_sleves_width_value.setText(AppConstants.orderList.getSleves_width());
        edt_sleves_length_back.setText(AppConstants.orderList.getLength_back());
        edt_slaves_length_forward.setText(AppConstants.orderList.getLength_forward());
        edt_sleves_width_forward.setText(AppConstants.orderList.getWidth_forward());
        tv_banda_type_value.setText(AppConstants.orderList.getBanda_type());
        tv_banda_length_value.setText(AppConstants.orderList.getBanda_length());
    }

    private void initView() {
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        tv_thoop_type_value = (TextView) findViewById(R.id.tv_thoop_type_value);
        tv_qalap_type_value = (TextView) findViewById(R.id.tv_qalap_type_value);
        tv_sleves_type_value = (TextView) findViewById(R.id.tv_sleves_type_value);
        tv_banda_type_value = (TextView) findViewById(R.id.tv_banda_type_value);
        tv_banda_length_value = (TextView) findViewById(R.id.tv_banda_length_value);


        lbl_thooptype = (TextView) findViewById(R.id.lbl_thooptype);
        tv_thoop_type = (TextView) findViewById(R.id.tv_thoop_type);
        lbl_qalaptype = (TextView) findViewById(R.id.lbl_qalaptype);
        tv_qalap_type = (TextView) findViewById(R.id.tv_qalap_type);
        lbl_width = (TextView) findViewById(R.id.lbl_width);
        lbl_height = (TextView) findViewById(R.id.lbl_height);
        lbl_height = (TextView) findViewById(R.id.lbl_height);
        tv_chest = (TextView) findViewById(R.id.tv_chest);
        tv_waist = (TextView) findViewById(R.id.tv_waist);
        lbl_pockettype = (TextView) findViewById(R.id.lbl_pockettype);
        tv_pocket_type = (TextView) findViewById(R.id.tv_pocket_type);
        tv_pocket_length = (TextView) findViewById(R.id.tv_pocket_length);
        tv_pocket_width = (TextView) findViewById(R.id.tv_pocket_width);
        tv_shoulder = (TextView) findViewById(R.id.tv_shoulder);
        lbl_slevestype = (TextView) findViewById(R.id.lbl_slevestype);
        tv_sleves_type = (TextView) findViewById(R.id.tv_sleves_type);
        tv_length = (TextView) findViewById(R.id.tv_length);
        tv_width = (TextView) findViewById(R.id.tv_width);
        tv_length_forward = (TextView) findViewById(R.id.tv_length_forward);
        tv_width_forward = (TextView) findViewById(R.id.tv_width_forward);
        tv_length_back = (TextView) findViewById(R.id.tv_length_back);
        lbl_bandatype = (TextView) findViewById(R.id.lbl_bandatype);
        tv_banda_type = (TextView) findViewById(R.id.tv_banda_type);
        tv_banda_length = (TextView) findViewById(R.id.tv_banda_length);

        edt_qalap_width = (EditText) findViewById(R.id.edt_qalap_width);
        edt_qalap_length = (EditText) findViewById(R.id.edt_qalap_length);
        edt_chest = (EditText) findViewById(R.id.edt_chest);
        edt_waist = (EditText) findViewById(R.id.edt_waist);
        edt_pocket_type = (EditText) findViewById(R.id.edt_pocket_type);
        edt_pocket_length_value = (EditText) findViewById(R.id.edt_pocket_length_value);
        edt_pocket_width_value = (EditText) findViewById(R.id.edt_pocket_width_value);
        edt_shoulder = (EditText) findViewById(R.id.edt_shoulder);
        edt_sleves_length_value = (EditText) findViewById(R.id.edt_sleves_length_value);
        edt_sleves_width_value = (EditText) findViewById(R.id.edt_sleves_width_value);
        edt_slaves_length_forward = (EditText) findViewById(R.id.edt_slaves_length_forward);
        edt_sleves_width_forward = (EditText) findViewById(R.id.edt_sleves_width_forward);
        edt_sleves_length_back = (EditText) findViewById(R.id.edt_sleves_length_back);


        rb_visible = (RadioButton) findViewById(R.id.rb_visible);
        rb_hidden = (RadioButton) findViewById(R.id.rb_hidden);
        rb_banda_visible = (RadioButton) findViewById(R.id.rb_banda_visible);
        rb_banda_hidden = (RadioButton) findViewById(R.id.rb_banda_hidden);
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_header_title.setText(AppConstants.orderList.getOrder_name().toUpperCase());
    }
}
