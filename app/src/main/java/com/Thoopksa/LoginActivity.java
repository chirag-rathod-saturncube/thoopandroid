package com.Thoopksa;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Thoopksa.api.GenerateOTPApi;
import com.Thoopksa.api.LabelAPi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.Preferences;

import java.util.Random;

public class LoginActivity extends AppCompatActivity {
    EditText edt_password, edt_fullname, edt_mobile;
    Button btnLogin;
    ImageView img_facebook, img_google, logo;
    RelativeLayout rel_forgot_pass, rel_register;
    TextView tv_register, tv_forgot, tv_employee_login, tv_chnge_language;
    private String TAG = getClass().getSimpleName();

    private static String arabicToDecimal(String number) {
        char[] chars = new char[number.length()];
        for (int i = 0; i < number.length(); i++) {
            char ch = number.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        Log.e("TAG", new String(chars));
        return new String(chars);
    }

    private static String arabicToEnglish(String number) {
        return number.replace("١", "1").replace("٢", "2").replace("", "3").replace("٤", "4").replace("٥", "5").replace("٦", "6").replace("٧", "7").replace("٨", "8").replace("٩", "9").replace("٠", "0");
    }

//    private void checkMobileApi() {
//        if (isValidate()) {
//            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), LoginActivity.this)) {
//                String envelope = "&phoneno=" + arabicToDecimal(edt_mobile.getText().toString());
//                new CheckMobileApi(envelope, LoginActivity.this, new CheckMobileApi.OnResultReceived() {
//                    @Override
//                    public void OnResult(String result) {
//                        generateOTP();
//                        AppConstants.mobile_number_english =  arabicToDecimal(edt_mobile.getText().toString());
//                        AppConstants.mobile_number_arabic = edt_mobile.getText().toString();
//                        Log.e(TAG, "Mobile number : " + AppConstants.mobile_number_english);
//                        Log.e("Error:   ", result);
//                    }
//                }, true);
//            }
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login1);
        initView();
        setIcon();
        setBaseWords();
        AppConstants.ACTION = AppConstants.LOGIN;
        edt_mobile.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.e("TAG", "arabicToDecimal :" + arabicToDecimal(edt_mobile.getText().toString()));
                    //checkMobileApi();
                    generateOTP();
                }
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("TAG", "arabicToDecimal :" + arabicToDecimal(edt_mobile.getText().toString()));
                        //checkMobileApi();
                        generateOTP();
                    }
                }, 300);
            }
        });


/*
        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
*/


/*
        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
*/
        tv_employee_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, EmployeeLoginActivity.class);
                startActivity(intent);
            }
        });

        if (Preferences.getValue_String(LoginActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            tv_chnge_language.setText("عربى");
        } else {
            tv_chnge_language.setText("English");
        }

        tv_chnge_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(LoginActivity.this, "language", Toast.LENGTH_SHORT).show();
                if (Preferences.getValue_String(LoginActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                    Preferences.setValue(LoginActivity.this, Preferences.LANGUAGE_CODE, "txt_arb");
                    String envelope = "&language=" + "txt_arb";
                    new LabelAPi(envelope, LoginActivity.this, new LabelAPi.OnResultReceived() {
                        @Override
                        public void onResult(String result) {
                            logo.setImageResource(R.drawable.app_logo);
                            setBaseWords();
                            tv_chnge_language.setText("English");
                            edt_mobile.setGravity(Gravity.RIGHT);
                            edt_password.setGravity(Gravity.RIGHT);
                        }
                    }, true);
                } else {
                    Preferences.setValue(LoginActivity.this, Preferences.LANGUAGE_CODE, "txt_eng");
                    String envelope = "&language=" + "txt_eng";
                    new LabelAPi(envelope, LoginActivity.this, new LabelAPi.OnResultReceived() {
                        @Override
                        public void onResult(String result) {
                            logo.setImageResource(R.drawable.app_logo_english);
                            setBaseWords();
                            tv_chnge_language.setText("عربى");
                            edt_mobile.setGravity(Gravity.LEFT);
                            edt_password.setGravity(Gravity.LEFT);
                        }
                    }, true);
                }
            }
        });
    }

    private void setIcon() {
        if (Preferences.getValue_String(LoginActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            logo.setImageResource(R.drawable.app_logo_english);
        } else {
            logo.setImageResource(R.drawable.app_logo);
        }
    }

//    private void login() {
//        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), LoginActivity.this)) {
//            String envelope = "&phoneno=" + AppConstants.mobile_number;
//            envelope = envelope + "&role_id=" + AppConstants.USER_ROLE;
//            new LoginApi(envelope, LoginActivity.this, new LoginApi.OnResultReceived() {
//                @Override
//                public void onResult(String result) "" {
//                    Log.e("Error:   ", result);
//                    Intent home = new Intent(LoginActivity.this, VerificationActivity.class);
//                    startActivity(home);
//                    finish();
//                }
//            }, true);
//        }
//    }

    private void setBaseWords() {
        edt_mobile.setHint(AppConstants.baseWords.get("Please_enter_mobile_number"));
        btnLogin.setText(AppConstants.baseWords.get("text_btn_login"));
        tv_employee_login.setText(AppConstants.baseWords.get("text_lbl_login_as_employee"));
    }

    private void initView() {
        edt_mobile = (EditText) findViewById(R.id.edt_mobile);
        edt_fullname = (EditText) findViewById(R.id.edt_fullname);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        img_facebook = (ImageView) findViewById(R.id.img_facebook);
        img_google = (ImageView) findViewById(R.id.img_google);
        rel_forgot_pass = (RelativeLayout) findViewById(R.id.rel_forgot_pass);
        rel_register = (RelativeLayout) findViewById(R.id.rel_register);
        tv_employee_login = (TextView) findViewById(R.id.tv_employee_login);
        tv_chnge_language = (TextView) findViewById(R.id.tv_chnge_language);
        logo = (ImageView) findViewById(R.id.logo);
    }
/*
    public void sendmessage() {
        AppConstants.verification_code = String.valueOf(getRandomNumber(1111, 9999));
        Log.e(TAG, "Verification Code :" + AppConstants.verification_code);
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), LoginActivity.this)) {
            String envelope = "&mobile=966553437652";
            envelope = envelope + "&password=thoop200";
            envelope = envelope + "&numbers=" + */
    /*"966"*//*
"91" + arabicToDecimal(edt_mobile.getText().toString());
            // Log.e(TAG, "Country Code : " + spn_countrycode.getSelectedItem());
            Log.e(TAG, "Mobile number : " + edt_mobile.getText().toString());

            envelope = envelope + "&sender=Thoop";

            if (Preferences.getValue_String(LoginActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                envelope = envelope + "&msg=" + getString(R.string.Please) + getString(R.string.enter) +
                        getString(R.string.the) + getString(R.string.verification)
                        + getString(R.string.number) + Unicode(AppConstants.verification_code);
            } else {
                envelope = envelope + "&msg=" + getString(R.string.Please_enter_the_Verification_number) + getString(R.string.space) + Unicode(AppConstants.verification_code);
            }


            envelope = envelope + "&timeSend=0";
            envelope = envelope + "&dateSend=0";
            envelope = envelope + "&applicationType=68";
            envelope = envelope + "&domainName=demo.Thoopksa.com";
            envelope = envelope + "&msgId=";

            new SMSApi(envelope, LoginActivity.this, new SMSApi.OnResultReceived() {
                @Override
                public void onResult(String result) {
                    AppConstants.ACTION = AppConstants.LOGIN;
                    Log.e(TAG, "Result : " + result);
                }
            }, true);
        }
    }
*/

    private int getRandomNumber(int min, int max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }

    public void generateOTP() {
        if (isValidate()) {
            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), LoginActivity.this)) {
                String envelope = "&mobile_number=" + AppConstants.country_code + arabicToDecimal(edt_mobile.getText().toString());
                Log.e(TAG, "Mobile number : " + arabicToDecimal(edt_mobile.getText().toString()));
                AppConstants.mobile_number_arabic = edt_mobile.getText().toString();
                AppConstants.mobile_number_english = AppConstants.arabicToDecimal(edt_mobile.getText().toString());
                if (Preferences.getValue_String(LoginActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                    envelope = envelope + "&language_id=" + AppConstants.lang_code_english;
                } else {
                    envelope = envelope + "&language_id=" + AppConstants.lang_code_arabic;
                }
                new GenerateOTPApi(envelope, LoginActivity.this, new GenerateOTPApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) {
                        AppConstants.ACTION = AppConstants.LOGIN;
                        Log.e(TAG, "Result : " + result);
                    }
                }, true);
            }
        }
    }

    public String convertToArabic(int value) {
        String newValue = (((((((((((value + "")
                .replaceAll("١", "1")).replaceAll("٢", "2"))
                .replaceAll("٣", "3")).replaceAll("٤", "4"))
                .replaceAll("٥", "5")).replaceAll("٦", "6"))
                .replaceAll("٧", "7")).replaceAll("٨", "8"))
                .replaceAll("٩", "9")).replaceAll("٠", "0"));
        return newValue;
    }


    public String Unicode(String s) {
        String value = "";
        for (int i = 0; i < s.length(); i++) {

            if (String.valueOf(s.charAt(i)).equals("1")) {
                value = value + "00000031";
            } else if (String.valueOf(s.charAt(i)).equals("2")) {
                value = value + "00000032";
            } else if (String.valueOf(s.charAt(i)).equals("3")) {
                value = value + "00000033";
            } else if (String.valueOf(s.charAt(i)).equals("4")) {
                value = value + "00000034";
            } else if (String.valueOf(s.charAt(i)).equals("5")) {
                value = value + "00000035";
            } else if (String.valueOf(s.charAt(i)).equals("6")) {
                value = value + "00000036";
            } else if (String.valueOf(s.charAt(i)).equals("7")) {
                value = value + "00000037";
            } else if (String.valueOf(s.charAt(i)).equals("8")) {
                value = value + "00000038";
            } else if (String.valueOf(s.charAt(i)).equals("9")) {
                value = value + "00000039";
            } else if (String.valueOf(s.charAt(i)).equals("0")) {
                value = value + "00000030";
            }
        }
        return value;
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";
        if (edt_mobile.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_mobile_number");
            flagValidate = false;
        } else if (!edt_mobile.getText().toString().startsWith("5")) {
            validateMsg = AppConstants.baseWords.get("Phone_number__should_start_with_5");
            flagValidate = false;
        } else if (!(edt_mobile.getText().toString().length() == 9)) {
            validateMsg = AppConstants.baseWords.get("Please_enter_valid_9_digit_phone_number");
            flagValidate = false;
        }
        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(LoginActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }
}
