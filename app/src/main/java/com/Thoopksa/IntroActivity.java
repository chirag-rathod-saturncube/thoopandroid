package com.Thoopksa;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.Thoopksa.adapter.SlideShowAdapter;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.Preferences;



public class IntroActivity extends AppCompatActivity {
    ViewPager view_pager;
    SlideShowAdapter slideShowAdapter;
    ImageView tv_next, tv_previous;
    TextView tv_skip;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        tv_next = (ImageView) findViewById(R.id.tv_next);
        tv_previous = (ImageView) findViewById(R.id.tv_previous);
        tv_skip = (TextView) findViewById(R.id.tv_skip);
        tv_skip.setText(AppConstants.baseWords.get("text_lbl_skip"));

        view_pager = (ViewPager) findViewById(R.id.view_pager);

        AppConstants.pic.add(R.drawable.splash_blank);
       // AppConstants.pic.add(R.drawable.splash);
       // AppConstants.pic.add(R.drawable.splash);
      //  AppConstants.pic.add(R.drawable.splash);

        slideShowAdapter = new SlideShowAdapter(IntroActivity.this, R.layout.intro_item);
        view_pager.setAdapter(slideShowAdapter);
        slideShowAdapter.notifyDataSetChanged();
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos = position;
                if (position > 0) {
                    tv_previous.setVisibility(View.VISIBLE);
                } else {
                    tv_previous.setVisibility(View.GONE);
                }

                /*if (position == 3) {
                    tv_next.setText("Finish");
                }else {
                    tv_next.setText("Next");
                }*/

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (pos == 3) {*/
                    Preferences.setValue(IntroActivity.this, Preferences.firstOpen, false);
                    startActivity(new Intent(IntroActivity.this, LoginActivity.class));
                    finish();
               /* } else {
                    view_pager.setCurrentItem(view_pager.getCurrentItem() + 1);
                }*/
            }
        });
        tv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                view_pager.setCurrentItem(view_pager.getCurrentItem() - 1);


            }
        });
        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.setValue(IntroActivity.this, Preferences.firstOpen, false);
                startActivity(new Intent(IntroActivity.this, LoginActivity.class));
                finish();

            }
        });

    }
}
