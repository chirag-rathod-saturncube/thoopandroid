package com.Thoopksa.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.Thoopksa.R;
import com.Thoopksa.api.LabelAPi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.Preferences;
import com.Thoopksa.moduls.Language;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class SelectLanguageAdapter extends RecyclerView.Adapter<SelectLanguageAdapter.CustomViewHolder> {
    private ArrayList<Language> arrayList;
    private Context mContext;
    List<Language> infos = null;
    private String TAG = getClass().getSimpleName();
    AlertDialog dialog;

    public SelectLanguageAdapter(Context context, List<Language> list, AlertDialog dialog) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.dialog = dialog;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        /*if (Preferences.getValue_String(mContext, Preferences.LANGUAGE_CODE).equals("txt_eng")) {*/
        Log.e(TAG, "IF : ");
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_select_language, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
       /* } else {
            Log.e(TAG,"ELSE : ");
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_select_language_arabic, null);
            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }*/
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Language list = infos.get(pos);
        Log.e(TAG, "code Api : " + list.getCode());
        Log.e(TAG, "code preference : " + Preferences.getValue_String(mContext, Preferences.LANGUAGE_CODE));

        customViewHolder.tv_title.setText(list.getLanguage_name());

        customViewHolder.chk_lang.setOnCheckedChangeListener(null);
        if (Preferences.getValue_String(mContext, Preferences.LANGUAGE_CODE).equals(list.getCode())) {
            customViewHolder.chk_lang.setChecked(true);
        } else {
            customViewHolder.chk_lang.setChecked(false);
        }

        customViewHolder.chk_lang.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (AppConstants.isNetworkAvailable("", "", mContext)) {
                    Preferences.setValue(mContext, Preferences.LANGUAGE_CODE, list.getCode());
                    notifyDataSetChanged();
                    String envelope = "&language=" + list.getCode();
                    new LabelAPi(envelope, mContext, new LabelAPi.OnResultReceived() {
                        @Override
                        public void onResult(String result)  {
                            dialog.dismiss();
                            ((Activity) mContext).finish();
                        }
                    }, true);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        CardView card_view;
        TextView tv_title;
        CheckBox chk_lang;

        public CustomViewHolder(View view) {
            super(view);
            this.card_view = (CardView) view.findViewById(R.id.card_view);
            this.tv_title = (TextView) view.findViewById(R.id.tv_title);
            this.chk_lang = (CheckBox) view.findViewById(R.id.chk_lang);
        }
    }


}

