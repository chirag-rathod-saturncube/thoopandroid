package com.Thoopksa.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Thoopksa.NewOrderActivity;
import com.Thoopksa.R;
import com.Thoopksa.api.DeleteOrderApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.Preferences;
import com.Thoopksa.moduls.OrderList;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class BookingDetailsAdapter extends RecyclerView.Adapter<BookingDetailsAdapter.CustomViewHolder> {
    private ArrayList<OrderList> arrayList;
    private Context mContext;
    List<OrderList> infos = null;
    private String TAG = getClass().getSimpleName();

    public BookingDetailsAdapter(Context context, ArrayList<OrderList> list) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;

    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_order_list, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final OrderList list = infos.get(pos);

        customViewHolder.tv_thoop_type.setText(list.getOrder_name().toUpperCase());
        customViewHolder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.orderList = list;

                if (Preferences.getValue_String(mContext, Preferences.ROLE_ID).equals(AppConstants.USER_ROLE)) {
                    AppConstants.ORDER_ACTION = AppConstants.ORDER_DETAIL;
                    Intent intent = new Intent(mContext, NewOrderActivity.class);
                    mContext.startActivity(intent);
                } else {
                    AppConstants.ORDER_ACTION = AppConstants.EDIT_ORDER;
                    Intent intent = new Intent(mContext, NewOrderActivity.class);
                    mContext.startActivity(intent);
                }


            }
        });
        if (Preferences.getValue_String(mContext, Preferences.ROLE_ID).equals(AppConstants.USER_ROLE)) {
            if (AppConstants.ACTION.equals(AppConstants.NEW_BOOKING)) {
                customViewHolder.delete.setVisibility(View.VISIBLE);
            } else if (AppConstants.ACTION.equals(AppConstants.COPY_BOOKING)) {
                customViewHolder.delete.setVisibility(View.VISIBLE);
            } else {
                customViewHolder.delete.setVisibility(View.GONE);
            }
        } else {
            customViewHolder.delete.setVisibility(View.VISIBLE);
        }


        customViewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Preferences.getValue_String(mContext, Preferences.ROLE_ID).equals(AppConstants.USER_ROLE)) {
                    infos.remove(pos);
                    notifyDataSetChanged();
                } else {


                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    dialog.dismiss();
                                    String param = "&order_id=" + list.getOrder_id();
                                    param = param + "&booking_id=" + AppConstants.booking_id;
                                    new DeleteOrderApi(param, mContext, new DeleteOrderApi.OnResultReceived() {
                                        @Override
                                        public void onResult(String result)  {
                                            infos.remove(pos);
                                            notifyDataSetChanged();
                                        }
                                    }, true);
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    dialog.dismiss();
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage(AppConstants.baseWords.get("Are_you_sure_want_to_delete") + list.getOrder_name() + "?")
                            .setPositiveButton(AppConstants.baseWords.get("Delete"), dialogClickListener)
                            .setNegativeButton(AppConstants.baseWords.get("CANCEL"), dialogClickListener)
                            .show();


                }
            }
        });
    }

    public String getOrderIds() {
        String ids = "";

        if (infos.size() > 0) {

            for (int i = 0; i < infos.size(); i++) {
                ids = ids + infos.get(i).getOrder_id() + ",";
            }
        }

        return ids;
    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout main;
        ImageView delete;
        TextView tv_thoop_type;


        public CustomViewHolder(View view) {
            super(view);
            this.delete = (ImageView) view.findViewById(R.id.delete);
            delete.setVisibility(View.GONE);
            this.main = (RelativeLayout) view.findViewById(R.id.main);

            this.tv_thoop_type = (TextView) view.findViewById(R.id.tv_thoop_type);


        }
    }
}

