package com.Thoopksa.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Thoopksa.BookingDetailsActivity;
import com.Thoopksa.NewBookingActivity;
import com.Thoopksa.R;
import com.Thoopksa.api.DeletebookingApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.Preferences;
import com.Thoopksa.moduls.BookingHistory;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class BookingHistoryAdapter extends RecyclerView.Adapter<BookingHistoryAdapter.CustomViewHolder> {
    private ArrayList<BookingHistory> arrayList;
    private Context mContext;
    List<BookingHistory> infos = null;
    private String TAG = getClass().getSimpleName();

    public BookingHistoryAdapter(Context context, ArrayList<BookingHistory> list) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;

    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_booking_history, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final BookingHistory list = infos.get(pos);

        customViewHolder.tv_order.setText(list.getBooking_name());

        customViewHolder.tv_bookin_date.setText(AppConstants.FormatDate(list.getBooking_date()) + " (" + list.getBooking_start_time() + " - " + list.getBooking_end_time() + ")");
        customViewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.IsBookingDetails = true;
                AppConstants.ACTION = AppConstants.HISTORY_DETAILS;
                AppConstants.booking_id = list.getBooking_id();
                Intent intent = new Intent(mContext, BookingDetailsActivity.class);
                mContext.startActivity(intent);
            }
        });
        customViewHolder.img_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
                alertDialog.setTitle(AppConstants.baseWords.get("Confirm_Copy"));
                alertDialog.setMessage(AppConstants.baseWords.get("msg_copybooking"));
                alertDialog.setPositiveButton(AppConstants.baseWords.get("Yes"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AppConstants.IsBookingDetails = true;
                        AppConstants.booking_id = list.getBooking_id();
                        AppConstants.ACTION = AppConstants.COPY_BOOKING;
                        Intent intent = new Intent(mContext, NewBookingActivity.class);
                        mContext.startActivity(intent);
                    }
                });
                alertDialog.setNegativeButton(AppConstants.baseWords.get("No"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();


            }
        });
        Log.e("Status", list.getStatus_booking());
        Log.e(TAG, "Status :" + list.getStatus_booking());
        Log.e(TAG, "copy booking : " + list.getCopy_booking());
        if (Preferences.getValue_String(mContext, Preferences.ROLE_ID).equals(AppConstants.USER_ROLE)) {
            customViewHolder.img_copy.setVisibility(View.VISIBLE);
            customViewHolder.img_delete.setVisibility(View.VISIBLE);

            if (list.getStatus_booking().equals("3")) {
                customViewHolder.status.setVisibility(View.VISIBLE);
                customViewHolder.img_copy.setVisibility(View.VISIBLE);
            } else if (list.getStatus_booking().equals("2")) {
                customViewHolder.status.setVisibility(View.GONE);
                customViewHolder.img_copy.setVisibility(View.GONE);
            }

        } else {
            customViewHolder.img_copy.setVisibility(View.GONE);
            customViewHolder.img_delete.setVisibility(View.GONE);

            if (list.getStatus_booking().equals("3")) {
                customViewHolder.status.setVisibility(View.VISIBLE);
                customViewHolder.img_copy.setVisibility(View.GONE);
            } else if (list.getStatus_booking().equals("2")) {
                if (list.getCopy_booking().equals("1")) {
                    customViewHolder.rel_booking.setBackgroundColor(Color.CYAN);
                } else {

                }
                customViewHolder.status.setVisibility(View.GONE);
                customViewHolder.img_copy.setVisibility(View.GONE);
            }

        }
        customViewHolder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(AppConstants.baseWords.get("Are_you_sure_want_to_delete_booking?"))
                        .setPositiveButton(AppConstants.baseWords.get("Delete"), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                String param = "&booking_id=" + list.getBooking_id();

                                new DeletebookingApi(param, mContext, new DeletebookingApi.OnResultReceived() {
                                    @Override
                                    public void OnResult(String result)  {
                                        infos.remove(pos);
                                        notifyDataSetChanged();
                                    }
                                }, true);
                            }
                        })
                        .setNegativeButton(AppConstants.baseWords.get("Cancle"), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();


//                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        switch (which) {
//                            case DialogInterface.BUTTON_POSITIVE:
//                                dialog.dismiss();
//                                String param = "&booking_id=" + list.getBooking_id();
//
//                                new DeletebookingApi(param, mContext, new DeletebookingApi.OnResultReceived() {
//                                    @Override
//                                    public void OnResult(String result)  {
//                                        infos.remove(pos);
//                                        notifyDataSetChanged();
//                                    }
//                                }, true);
//                                break;
//
//                            case DialogInterface.BUTTON_NEGATIVE:
//                                dialog.dismiss();
//                                break;
//                        }
//                    }
//                };

            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        CardView card_view;

        TextView tv_order, tv_bookin_date, status;
        ImageView img_copy, img_delete;
        RelativeLayout rel_booking;


        public CustomViewHolder(View view) {
            super(view);
            this.card_view = (CardView) view.findViewById(R.id.card_view);
            this.rel_booking = (RelativeLayout) view.findViewById(R.id.rel_booking);

            this.tv_order = (TextView) view.findViewById(R.id.tv_order);
            this.status = (TextView) view.findViewById(R.id.status);
            this.tv_bookin_date = (TextView) view.findViewById(R.id.tv_bookin_date);
            this.img_copy = (ImageView) view.findViewById(R.id.img_copy);
            this.img_delete = (ImageView) view.findViewById(R.id.img_delete);


        }
    }
}

