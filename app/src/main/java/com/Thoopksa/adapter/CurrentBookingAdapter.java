package com.Thoopksa.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Thoopksa.BookingDetailsActivity;
import com.Thoopksa.R;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.moduls.CurrentBooking;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pc4 on 5/30/2016.
 */
public class CurrentBookingAdapter extends RecyclerView.Adapter<CurrentBookingAdapter.CustomViewHolder> {
    private ArrayList<CurrentBooking> arrayList;
    private Context mContext;
    List<CurrentBooking> infos = null;
    private String TAG = getClass().getSimpleName();


    public CurrentBookingAdapter(Context context, List<CurrentBooking> list) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_current_booking, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final CurrentBooking list = infos.get(pos);

        customViewHolder.tv_order.setText(list.getBooking_name());




        customViewHolder.tv_bookin_date.setText(AppConstants.FormatDate(list.getBooking_date()) + " (" + list.getBooking_start_time() + " - " + list.getBooking_end_time() + ")");
        customViewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.IsBookingDetails = true;
                AppConstants.ACTION = AppConstants.BOOKING_DETAILS;
                AppConstants.booking_id=list.getBooking_id();
                Intent intent = new Intent(mContext, BookingDetailsActivity.class);
                mContext.startActivity(intent);


            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        CardView card_view;

        TextView tv_order, tv_bookin_date;


        public CustomViewHolder(View view) {
            super(view);
            this.card_view = (CardView) view.findViewById(R.id.card_view);

            this.tv_order = (TextView) view.findViewById(R.id.tv_order);
            this.tv_bookin_date = (TextView) view.findViewById(R.id.tv_bookin_date);

        }
    }
}

