package com.Thoopksa.common;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by pc4 on 11/19/2016.
 */

public class Preferences {
    public static final String APP_PREFERENCES = AppConstants.APP_NAME;
    public static final String CONNECT_TO = "CONNECT_TO";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_PIC= "USER_PIC";
    public static final String USER_ID= "USER_ID";
    public static final String EMAIL= "EMAIL_ID";
    public static final String PASSWORD= "PASSWORD";
    public static final String ROLE_ID= "ROLE_ID";
    public static final String FULL_NAME= "FULL_NAME";
    public static final String PHONE_NO= "PHONE_NO";
    public static final String firstOpen = "firstopen";
    public static final String LANGUAGE_CODE= "LANGUAGE_CODE";
    public static final String USER_JWT = "user_jwt";



    public static void setValue(Context context, String Key, String Value) {
        SharedPreferences settings = context.getSharedPreferences(
                APP_PREFERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Key, Value);
        editor.commit();
    }
    public static void setValue(Context context, String Key, boolean Value) {
        SharedPreferences settings = context.getSharedPreferences(
                APP_PREFERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(Key, Value);
        editor.commit();
    }
    public static boolean getValue_Boolean(Context context, String Key, boolean Default) {
        if (context != null) {
            SharedPreferences settings = context.getSharedPreferences(APP_PREFERENCES, 0);
            return settings.getBoolean(Key, Default);
        } else {
            return false;
        }
    }

    public static String getValue_String(Context context, String Key) {
        SharedPreferences settings = context.getSharedPreferences(
                APP_PREFERENCES, 0);
        return settings.getString(Key, "");
    }
}
