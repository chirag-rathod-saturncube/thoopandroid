package com.Thoopksa.common;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class WebServices {
    public static final String RESPONSE_UNWANTED = "UNWANTED";

    public static String SECRET_KEY = "saturncube_thoop_2018";
    public static String COMMON_JWT = "c2F0dXJuY3ViZXRob29w";

    //private static final String BASE_URL = "http://demo.saturncube.com/thoop/api.php?";
    //private static final String BASE_URL = "http://thoopksa.com/API/api.php?";
    private static final String BASE_URL = "http://thoopksa.com/API/api_1.php?";

    public static final String REGISTER_URL = BASE_URL + "action=register";
    public static final String LOGIN_URL = BASE_URL + "action=login";
    public static final String EMPLOYEE_LOGIN_URL = BASE_URL + "action=employeeLogin";
    public static final String CURRENTBOOKING_API = BASE_URL + "action=view_current_bookinglist";
    public static final String BOOKINGHISTORY_API = BASE_URL + "action=view_history_bookinglist";
    public static final String NEWBOOKING_API = BASE_URL + "action=add_booking";
    public static final String NEW_ORDER_API = BASE_URL + "action=addorder";
    public static final String DELETE_ORDER_API = BASE_URL + "action=deleteorder";
    public static final String EDIT_ORDER_API = BASE_URL + "action=updateorder";
    public static final String FORGOT_PWD_URL = BASE_URL + "action=confirmForgetPass";
    public static final String BOOKING_DETAILS_API = BASE_URL + "action=view_current_bookingdetails";
    public static final String GET_ORDERLIST_API = BASE_URL + "action=getorderlist";
    public static final String DELETE_BOOKING = BASE_URL + "action=deletebooking";
    public static final String GET_QALAP = BASE_URL + "action=getqalap";
    public static final String DONE_BOOKING = BASE_URL + "action=editdoneorder";
    public static final String COPY_BOOKING = BASE_URL + "action=copy_booking";
    public static final String CHANGE_PASSWORD = BASE_URL + "action=changePassword";
    public static final String VIEW_PROFILE = BASE_URL + "action=viewProfile";
    public static final String EDIT_MOBILE = BASE_URL + "action=editmobileno";
    public static final String CREATE_PDF = BASE_URL + "action=createpdf";
    public static final String LABEL_API = BASE_URL + "action=getLabel";
    public static final String LANGUAGES_API = BASE_URL + "action=getLanguages";
    public static final String EMAILVALIDATE = BASE_URL + "action=checkEmail";
    public static final String CREATEAREBICPDF = BASE_URL + "action=createarabicpdf";
    public static final String CHECKMOBILE_API = BASE_URL + "action=checkMobile";
    public static final String ADD_PROFILE_API = BASE_URL + "action=addProfile";
    public static final String GET_DATE_TIMMING = BASE_URL + "action=getDateTimming";
    public static final String LOGOUT_API = BASE_URL + "action=logout";
    public static final String GENERATE_OTP_API = BASE_URL + "action=generateOTP";
    public static final String VERIFY_OTP_API = BASE_URL + "action=verifyOTP";

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

    public static void generateJwt(Context mContext, String userHash) {
        Log.e("TAG", "==============================JWT token info===================================");
        String key = SECRET_KEY;
        try {
            JSONObject payload = new JSONObject();
            payload.put("userhash", Integer.parseInt(userHash));
            String jwtString = Jwts.builder()
                    .setHeaderParam("alg", "HS256")
                    .setHeaderParam("typ", "JWT")
                    .setPayload(payload.toString())
                    .signWith(SignatureAlgorithm.HS256, key.getBytes(Charset.forName("UTF-8")))
                    .compact();

            Preferences.setValue(mContext, Preferences.USER_JWT, jwtString);

            Log.e("TAG", "JWT payload: " + payload);
            Log.e("TAG", "JWT token : " + Preferences.getValue_String(mContext, Preferences.USER_JWT));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}