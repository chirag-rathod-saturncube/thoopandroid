package com.Thoopksa.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.Thoopksa.R;
import com.Thoopksa.moduls.BookingDetails;
import com.Thoopksa.moduls.BookingHistory;
import com.Thoopksa.moduls.CurrentBooking;
import com.Thoopksa.moduls.Demo;
import com.Thoopksa.moduls.Filter;
import com.Thoopksa.moduls.GetDateTime;
import com.Thoopksa.moduls.Language;
import com.Thoopksa.moduls.OrderList;
import com.Thoopksa.moduls.ViewProfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by pc4 on 2/24/2017.
 */

public class AppConstants {
    public static final String APP_CONSTANT = "APP_CONSTANT";

    public static final String FACEBOOK = "FACEBOOK";
    public static final String SFINDIT = "SFINDIT";

    public static final String APP_NAME = "Thoop";

    //public static final String country_code = "91";
    public static final String country_code = "966";

    public static String registerInfo = "";
    public static int regStatus = 0;
    public static int lang_code_english = 1;
    public static int lang_code_arabic = 2;
    public static String check_mobile_status = "";
    public static String isRegistered = "";
    public static String SMS_status = "";


    public static ArrayList<CurrentBooking> currentbookingArraylist = new ArrayList<>();
    public static ArrayList<BookingHistory> bookinghistoryArraylist = new ArrayList<>();
    public static ArrayList<OrderList> orderlistArraylist = new ArrayList<>();
    public static ArrayList<Language> languageArrayList = new ArrayList<>();

    public static ArrayList<Filter> qalapList = new ArrayList<>();
    public static ArrayList<Filter> bandaList = new ArrayList<>();
    public static ArrayList<Filter> thoopList = new ArrayList<>();
    public static ArrayList<Filter> slevesList = new ArrayList<>();
    public static ArrayList<Integer> pic = new ArrayList<Integer>();

    public static ArrayList<Demo> demoArrayList = new ArrayList<>();


    public static String encodeType = "UTF-8";
    public static String map_lat = "";
    public static String map_lng = "";
    public static String map_address;
    public static String map_country;
    public static String ACTION = "";
    public static String ORDER_ACTION = "";
    public static String booking_id = "";
    public static String pdfpath = "";
    public static String mobile_number_english = "";
    public static String mobile_number_arabic = "";

    public static final String NEW_BOOKING = "NEW_BOOKING";
    public static final String COPY_BOOKING = "COPY_BOOKING";
    public static final String BOOKING_DETAILS = "BOOKING_DETAILS";
    public static final String HISTORY_DETAILS = "HISTORY_DETAILS";
    public static final String NEW_ORDER = "NEW_ORDER";
    public static final String EDIT_ORDER = "EDIT_ORDER";
    public static final String ORDER_DETAIL = "ORDER_DETAIL";
    public static final String REGISTER = "REGISTER";
    public static final String LOGIN = "LOGIN";
    public static final String CHANGE_MOBILE = "CHANGE_MOBILE";
    public static final String VERIFICATION = "VERIFICATION";

    public static final String USER_ROLE = "2";
    public static final String EMPLOYEE_ROLE = "1";

    public static boolean IscurrentBooking = true;
    public static boolean IshistoryBooking = true;
    public static boolean IsBookingDetails = true;

    public static HashMap<String, String> baseWords = new HashMap<>();

    public static BookingDetails bookingDetails = new BookingDetails();
    public static ViewProfile viewprofile = new ViewProfile();
    public static OrderList orderList = new OrderList();
    public static GetDateTime getDateTime = new GetDateTime();

    public static String code_fullname = "";
    public static String code_email = "";
    public static String code_mobile = "";
    public static String code_password = "";
    public static String code_confirmpassword = "";
    public static String verification_code = "";
    public static String mobile_code = "";
    public static int status = 0;
    //public static boolean isNotificationScreenOpen = false;
    //public static ArrayList<GroupMsg> groupMsgArrayList = new ArrayList<>(5);

    public static boolean isNetworkAvailable(final String title, final String message, final Context context) {
        com.Thoopksa.common.Config.getmycontext(context);
        if (com.Thoopksa.common.Config.oConnectivityManager.getActiveNetworkInfo() != null && com.Thoopksa.common.Config.oConnectivityManager.getActiveNetworkInfo().isAvailable() && com.Thoopksa.common.Config.oConnectivityManager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.dialog_internet_connection, null);
            dialogBuilder.setView(dialogView);
            final AlertDialog b = dialogBuilder.create();

            b.show();


            final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.img_close);
            final TextView tvTitle = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
            final TextView tvMsg = (TextView) dialogView.findViewById(R.id.tv_dialog_msg);
            final TextView tvOk = (TextView) dialogView.findViewById(R.id.tv_ok);

            tvTitle.setText("Retry");
            tvOk.setText("OK");
            tvMsg.setText("Please check internet connectivity");

            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();
                }
            });

            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();

                }
            });


            tvOk.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        tvOk.setTextColor(Color.WHITE);
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        tvOk.setTextColor(Color.rgb(6, 190, 188));
                    }
                    return false;
                }
            });

            return false;
        }
    }

    public static void clearPreferences(Context context) {
        Preferences.setValue(context, Preferences.USER_ID, "");
        Preferences.setValue(context, Preferences.EMAIL, "");
        Preferences.setValue(context, Preferences.PASSWORD, "");
        Preferences.setValue(context, Preferences.ROLE_ID, "");
        Preferences.setValue(context, Preferences.FULL_NAME, "");
        Preferences.setValue(context, Preferences.PHONE_NO, "");
        Preferences.setValue(context, Preferences.LANGUAGE_CODE, "");
        Preferences.setValue(context, Preferences.USER_JWT, "");

    }

    public static boolean validateEmail(String email) {
        Pattern pattern = Pattern
                .compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}");
        // Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static Bitmap getRescaledBitmap(String filePath) {
        Bitmap bitmap = null;
        try {
            int targetWidth = 480; // your arbitrary fixed limit
            int targetHeight = 480;
            File image = new File(filePath);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), options);
            int ImageWidth = bitmap.getWidth();
            int ImageHeight = bitmap.getHeight();
            Log.e("before WxH", ImageWidth + "x" + ImageHeight);
            if (ImageHeight >= ImageWidth) {
                // targetWidth = 480;
                targetHeight = (int) (ImageHeight * targetWidth / (double) ImageWidth); // casts

                if (ImageWidth > targetWidth) {
                    bitmap = com.Thoopksa.common.ScalingUtilities.decodeFile(image.getAbsolutePath(), targetWidth, targetHeight);
                    bitmap = com.Thoopksa.common.ScalingUtilities.createScaledBitmap(bitmap,
                            targetWidth, targetHeight);
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, imagefile);
                } else {
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, imagefile);
                }

                Log.e("after WxH", bitmap.getWidth() + "x" + bitmap.getHeight());

            } else {

                targetWidth = (int) (ImageWidth * targetHeight / (double) ImageHeight); // casts

                if (ImageHeight > targetHeight) {
                    bitmap = com.Thoopksa.common.ScalingUtilities.decodeFile(
                            image.getAbsolutePath(), targetWidth, targetHeight);
                    bitmap = com.Thoopksa.common.ScalingUtilities.createScaledBitmap(bitmap,
                            targetWidth, targetHeight);
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, imagefile);
                } else {
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, imagefile);
                }
                Log.e("WxH", bitmap.getWidth() + "x" + bitmap.getHeight());

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return bitmap;

    }

    public static String FormatDate(String date) {
        String formateddate = "";

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date newDate = null;
        try {
            newDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat format1;


        if (date.endsWith("1") && !date.endsWith("11"))
            format1 = new SimpleDateFormat("d'st' MM, yyyy");
        else if (date.endsWith("2") && !date.endsWith("12"))
            format1 = new SimpleDateFormat("d'nd' MM, yyyy");
        else if (date.endsWith("3") && !date.endsWith("13"))
            format1 = new SimpleDateFormat("d'rd' MM, yyyy");
        else
            format1 = new SimpleDateFormat("d'th' MM, yyyy");
        formateddate = format1.format(newDate);
        return formateddate;
    }

    //    public static String FormatDate(String date) {
//        String formateddate = "";
//
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        Date newDate = null;
//        try {
//            newDate = format.parse(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        SimpleDateFormat format1;
//
//
//        if (date.endsWith("1") && !date.endsWith("11"))
//            format1 = new SimpleDateFormat("d'st' MMM, yyyy");
//        else if (date.endsWith("2") && !date.endsWith("12"))
//            format1 = new SimpleDateFormat("d'nd' MMM, yyyy");
//        else if (date.endsWith("3") && !date.endsWith("13"))
//            format1 = new SimpleDateFormat("d'rd' MMM, yyyy");
//        else
//            format1 = new SimpleDateFormat("d'th' MMM, yyyy");
//        formateddate = format1.format(newDate);
//        return formateddate;
//    }
    public static String arabicToDecimal(String number) {
        char[] chars = new char[number.length()];
        for (int i = 0; i < number.length(); i++) {
            char ch = number.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        return new String(chars);
    }
}
