package com.Thoopksa;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.Thoopksa.api.LabelAPi;
import com.Thoopksa.api.LanguagesApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.Preferences;
import com.google.firebase.iid.FirebaseInstanceId;


public class SplashActivity extends AppCompatActivity {
    private final int TIMEOUT = 2000;
    String TAG = getClass().getSimpleName();
    private static String userEmail;
    private static String id, languageCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Log.e(TAG, "SplashActivity(){...}");
        Log.e(TAG, "FCM ID : " + FirebaseInstanceId.getInstance().getToken());

        if (Preferences.getValue_Boolean(SplashActivity.this, Preferences.firstOpen, true)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (AppConstants.isNetworkAvailable("", "", SplashActivity.this)) {
                        new LanguagesApi("", SplashActivity.this, new LanguagesApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) {
                                languageCode = Preferences.getValue_String(SplashActivity.this, Preferences.LANGUAGE_CODE);
                                if (languageCode != null) {

                                    if (languageCode.equals("")) {
                                        //Preferences.setValue(SplashActivity.this, Preferences.LANGUAGE_CODE, AppConstants.languageArrayList.get(0).getCode());
                                        Preferences.setValue(SplashActivity.this, Preferences.LANGUAGE_CODE, AppConstants.languageArrayList.get(1).getCode());
                                    }


                                    String envelope = "&language=" + Preferences.getValue_String(SplashActivity.this, Preferences.LANGUAGE_CODE);
                                    new LabelAPi(envelope, SplashActivity.this, new LabelAPi.OnResultReceived() {
                                        @Override
                                        public void onResult(String result){
                                            startActivity(new Intent(SplashActivity.this, IntroActivity.class));
                                            finish();
                                        }
                                    }, false);

                                }
                            }
                        }, false);
                    }


                }
            }, TIMEOUT);
        } else {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onLogin();
                }
            }, TIMEOUT);

        }
    }

    private void onLogin() {

        id = Preferences.getValue_String(getBaseContext(), Preferences.USER_ID);

        if (AppConstants.isNetworkAvailable("", "", SplashActivity.this)) {
            new LanguagesApi("", SplashActivity.this, new LanguagesApi.OnResultReceived() {
                @Override
                public void onResult(String result){
                    languageCode = Preferences.getValue_String(SplashActivity.this, Preferences.LANGUAGE_CODE);
                    if (languageCode != null) {

                        if (languageCode.equals("")) {
                            Preferences.setValue(SplashActivity.this, Preferences.LANGUAGE_CODE, AppConstants.languageArrayList.get(0).getCode());
                        }


                        String envelope = "&language=" + Preferences.getValue_String(SplashActivity.this, Preferences.LANGUAGE_CODE);
                        new LabelAPi(envelope, SplashActivity.this, new LabelAPi.OnResultReceived() {
                            @Override
                            public void onResult(String result)  {
                                if (id != null) {
                                    if (!id.equals("")) {
                                        AppConstants.IscurrentBooking = true;
                                        Intent Intent_home = new Intent(SplashActivity.this, HomeActivity.class);
                                        startActivity(Intent_home);
                                        finish();
                                    } else {
                                        Intent Intent_login = new Intent(SplashActivity.this, LoginActivity.class);
                                        startActivity(Intent_login);
                                        finish();
                                    }
                                } else {
                                    Intent Intent_login = new Intent(SplashActivity.this, LoginActivity.class);
                                    startActivity(Intent_login);
                                    finish();
                                }
                            }
                        }, false);

                    }
                }
            }, false);
        }
    }
}
