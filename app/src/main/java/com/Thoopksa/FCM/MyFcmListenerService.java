package com.Thoopksa.FCM;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.Thoopksa.HomeActivity;
import com.Thoopksa.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by Saturncube10 on 22-Jul-16.
 */
public class MyFcmListenerService extends FirebaseMessagingService {
    private String TAG = getClass().getSimpleName();
    private Context context;
    public static int NOTIFICATION_COUNT = 10;


    @Override
    public void onMessageReceived(RemoteMessage message) {
        context = getApplicationContext();
        String from = message.getFrom();
        Map data = message.getData();
        Log.e("Notification ", " Data : " + data.toString());
        Log.e("From ", " From Data  : " + from);
        String msg = data.get("msg").toString();

        sendNotification(msg);
    }

    private void sendNotification(String messageBody) {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent notificationIntent;

        notificationIntent = new Intent(this, HomeActivity.class);
        notificationIntent.putExtra("NotiClick", true);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent intent = PendingIntent.getActivity(this, requestID, notificationIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat
                .Builder(this)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setSmallIcon(R.drawable.ic_noti)
                //.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(getString(R.string.app_name))
                .setSound(defaultSoundUri)
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentIntent(intent)
                .setColor(Color.parseColor("#000000"))
                .setAutoCancel(true);
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 100, 1500};
        if (!messageBody.equals("null")) {
            v.vibrate(pattern, -1);
            notificationManager.notify(NOTIFICATION_COUNT, builder.build());
            NOTIFICATION_COUNT = 1 + NOTIFICATION_COUNT;
        }
    }
}