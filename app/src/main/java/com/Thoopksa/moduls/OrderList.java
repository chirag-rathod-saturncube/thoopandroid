package com.Thoopksa.moduls;

/**
 * Created by pc4 on 3/1/2017.
 */

public class OrderList {
   String booking_id,user_id,booking_name,booking_date,booking_start_time,booking_end_time,latitude,longitude,address,order_id,complete_booking,
           crt_date,new_update_date,active_flag,measurement_id,order_name,amount,finish_order,done_order,qalap_type,qalap_length,qalap_width,
           qalap_h_v,thoop_type,chest,waist,pocket_type,pocket_length,pocket_width,shoulder,sleves_type,sleves_length,sleves_width,length_forward,
           width_forward,length_back,banda_type,banda_length,banda_h_v,measur_notes;

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBooking_name() {
        return booking_name;
    }

    public void setBooking_name(String booking_name) {
        this.booking_name = booking_name;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getBooking_start_time() {
        return booking_start_time;
    }

    public void setBooking_start_time(String booking_start_time) {
        this.booking_start_time = booking_start_time;
    }

    public String getBooking_end_time() {
        return booking_end_time;
    }

    public void setBooking_end_time(String booking_end_time) {
        this.booking_end_time = booking_end_time;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getComplete_booking() {
        return complete_booking;
    }

    public void setComplete_booking(String complete_booking) {
        this.complete_booking = complete_booking;
    }

    public String getCrt_date() {
        return crt_date;
    }

    public void setCrt_date(String crt_date) {
        this.crt_date = crt_date;
    }

    public String getNew_update_date() {
        return new_update_date;
    }

    public void setNew_update_date(String new_update_date) {
        this.new_update_date = new_update_date;
    }

    public String getActive_flag() {
        return active_flag;
    }

    public void setActive_flag(String active_flag) {
        this.active_flag = active_flag;
    }

    public String getMeasurement_id() {
        return measurement_id;
    }

    public void setMeasurement_id(String measurement_id) {
        this.measurement_id = measurement_id;
    }

    public String getOrder_name() {
        return order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFinish_order() {
        return finish_order;
    }

    public void setFinish_order(String finish_order) {
        this.finish_order = finish_order;
    }

    public String getDone_order() {
        return done_order;
    }

    public void setDone_order(String done_order) {
        this.done_order = done_order;
    }

    public String getQalap_type() {
        return qalap_type;
    }

    public void setQalap_type(String qalap_type) {
        this.qalap_type = qalap_type;
    }

    public String getQalap_length() {
        return qalap_length;
    }

    public void setQalap_length(String qalap_length) {
        this.qalap_length = qalap_length;
    }

    public String getQalap_width() {
        return qalap_width;
    }

    public void setQalap_width(String qalap_width) {
        this.qalap_width = qalap_width;
    }

    public String getQalap_h_v() {
        return qalap_h_v;
    }

    public void setQalap_h_v(String qalap_h_v) {
        this.qalap_h_v = qalap_h_v;
    }

    public String getThoop_type() {
        return thoop_type;
    }

    public void setThoop_type(String thoop_type) {
        this.thoop_type = thoop_type;
    }

    public String getChest() {
        return chest;
    }

    public void setChest(String chest) {
        this.chest = chest;
    }

    public String getWaist() {
        return waist;
    }

    public void setWaist(String waist) {
        this.waist = waist;
    }

    public String getPocket_type() {
        return pocket_type;
    }

    public void setPocket_type(String pocket_type) {
        this.pocket_type = pocket_type;
    }

    public String getPocket_length() {
        return pocket_length;
    }

    public void setPocket_length(String pocket_length) {
        this.pocket_length = pocket_length;
    }

    public String getPocket_width() {
        return pocket_width;
    }

    public void setPocket_width(String pocket_width) {
        this.pocket_width = pocket_width;
    }

    public String getShoulder() {
        return shoulder;
    }

    public void setShoulder(String shoulder) {
        this.shoulder = shoulder;
    }

    public String getSleves_type() {
        return sleves_type;
    }

    public void setSleves_type(String sleves_type) {
        this.sleves_type = sleves_type;
    }

    public String getSleves_length() {
        return sleves_length;
    }

    public void setSleves_length(String sleves_length) {
        this.sleves_length = sleves_length;
    }

    public String getSleves_width() {
        return sleves_width;
    }

    public void setSleves_width(String sleves_width) {
        this.sleves_width = sleves_width;
    }

    public String getLength_forward() {
        return length_forward;
    }

    public void setLength_forward(String length_forward) {
        this.length_forward = length_forward;
    }

    public String getWidth_forward() {
        return width_forward;
    }

    public void setWidth_forward(String width_forward) {
        this.width_forward = width_forward;
    }

    public String getLength_back() {
        return length_back;
    }

    public void setLength_back(String length_back) {
        this.length_back = length_back;
    }

    public String getBanda_type() {
        return banda_type;
    }

    public void setBanda_type(String banda_type) {
        this.banda_type = banda_type;
    }

    public String getBanda_length() {
        return banda_length;
    }

    public void setBanda_length(String banda_length) {
        this.banda_length = banda_length;
    }

    public String getBanda_h_v() {
        return banda_h_v;
    }

    public void setBanda_h_v(String banda_h_v) {
        this.banda_h_v = banda_h_v;
    }

    public String getMeasur_notes() {
        return measur_notes;
    }

    public void setMeasur_notes(String measur_notes) {
        this.measur_notes = measur_notes;
    }
}
