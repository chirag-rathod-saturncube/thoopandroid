package com.Thoopksa.moduls;

/**
 * Created by Saturncube-5 on 19-Jan-18.
 */

public class GetDateTime {

    String setting_id,min_date,start_time,end_time,start_hours,start_minutes,end_hours,end_minutes;

    public String getSetting_id() {
        return setting_id;
    }

    public void setSetting_id(String setting_id) {
        this.setting_id = setting_id;
    }

    public String getMin_date() {
        return min_date;
    }

    public void setMin_date(String min_date) {
        this.min_date = min_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getStart_hours() {
        return start_hours;
    }

    public void setStart_hours(String start_hours) {
        this.start_hours = start_hours;
    }

    public String getStart_minutes() {
        return start_minutes;
    }

    public void setStart_minutes(String start_minutes) {
        this.start_minutes = start_minutes;
    }

    public String getEnd_hours() {
        return end_hours;
    }

    public void setEnd_hours(String end_hours) {
        this.end_hours = end_hours;
    }

    public String getEnd_minutes() {
        return end_minutes;
    }

    public void setEnd_minutes(String end_minutes) {
        this.end_minutes = end_minutes;
    }
}
