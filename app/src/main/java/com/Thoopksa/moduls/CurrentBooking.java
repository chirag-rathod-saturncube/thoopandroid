package com.Thoopksa.moduls;

/**
 * Created by pc4 on 3/1/2017.
 */

public class CurrentBooking {
    String booking_date, booking_name,booking_id,booking_start_time,booking_end_time,complete_booking;

    public String getBooking_start_time() {
        return booking_start_time;
    }

    public void setBooking_start_time(String booking_start_time) {
        this.booking_start_time = booking_start_time;
    }

    public String getBooking_end_time() {
        return booking_end_time;
    }

    public void setBooking_end_time(String booking_end_time) {
        this.booking_end_time = booking_end_time;
    }

    public String getComplete_booking() {
        return complete_booking;
    }

    public void setComplete_booking(String complete_booking) {
        this.complete_booking = complete_booking;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getBooking_name() {
        return booking_name;
    }

    public void setBooking_name(String booking_name) {
        this.booking_name = booking_name;
    }
}
