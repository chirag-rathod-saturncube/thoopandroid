package com.Thoopksa;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.Thoopksa.api.EditOrderApi;
import com.Thoopksa.api.NewOrderApi;
import com.Thoopksa.api.SMSApi;
import com.Thoopksa.api.getQalapApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.Preferences;
import com.Thoopksa.moduls.Filter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class NewOrderActivity extends AppCompatActivity {
    String TAG = getClass().getSimpleName();


    Spinner spn_thoop_type, spn_qalap_type, spn_sleves_type, spn_banda_type;

    EditText edt_qalap_width, edt_qalap_length, edt__qalap_chest, edt__edt_waist, edt_pocket_type,
            edt_shoulder, edt_length_forward, edt_width_forward, edt_length_back, tv_banda_length_value,
            edt_pocket_width, edt_pocket_length, edt_sleves_width, edt_sleves_length, edt_notes;
    RadioButton rb__qalap_visible, rb_qalap_hidden, rb_banda_visible, rb_banda_hidden;
    ArrayAdapter<Filter> thoop_adapter, qalap_adapter, sleves_adapter, banda_adapter;

    Button btnbooking, btn_cancle;
    ImageView img_back;
    TextView tv_header_title, lbl_thooptype, tv_thoop_type, lbl_qalaptype, tv_qalap_type, tv_length, tv_width, tv_chest, tv_waist, lbl_pockettype,
            tv_pocket_type, tv_pocket_length, tv_pocket_width, tv_shoulder, lbl_slevestype, tv_sleves_type, tv_sleves_length, tv_sleves_width,
            tv_length_forward, tv_width_forward, tv_length_back, lbl_bandatype, tv_banda_type, tv_banda_length;


    String thoop_type = "";
    String qalap_type = "";
    String sleves_type = "";
    String banda_type = "";
    String qalap_visible = "1";
    String banda_visible = "1";
    RelativeLayout rel_five;
    private int activity_new_order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);
        initView();
        setBaseWords();
        RadioGroup qalap_radio = (RadioGroup) findViewById(R.id.qalap_radio);
        RadioGroup banda_radio = (RadioGroup) findViewById(R.id.banda_radio);

        if (Preferences.getValue_String(NewOrderActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            tv_waist.setGravity(Gravity.LEFT);
            edt_qalap_width.setGravity(Gravity.LEFT);
            edt_qalap_length.setGravity(Gravity.LEFT);
            edt__qalap_chest.setGravity(Gravity.LEFT);
            edt__edt_waist.setGravity(Gravity.LEFT);
            edt_pocket_type.setGravity(Gravity.LEFT);
            edt_shoulder.setGravity(Gravity.LEFT);
            edt_length_forward.setGravity(Gravity.LEFT);
            edt_width_forward.setGravity(Gravity.LEFT);
            edt_length_back.setGravity(Gravity.LEFT);
            tv_banda_length_value.setGravity(Gravity.LEFT);
            edt_pocket_width.setGravity(Gravity.LEFT);
            edt_pocket_length.setGravity(Gravity.LEFT);
            edt_sleves_width.setGravity(Gravity.LEFT);
            edt_sleves_length.setGravity(Gravity.LEFT);
            edt_notes.setGravity(Gravity.LEFT);

        } else {
            tv_waist.setGravity(Gravity.RIGHT);
            edt_qalap_width.setGravity(Gravity.RIGHT);
            edt_qalap_length.setGravity(Gravity.RIGHT);
            edt__qalap_chest.setGravity(Gravity.RIGHT);
            edt__edt_waist.setGravity(Gravity.RIGHT);
            edt_pocket_type.setGravity(Gravity.RIGHT);
            edt_shoulder.setGravity(Gravity.RIGHT);
            edt_length_forward.setGravity(Gravity.RIGHT);
            edt_width_forward.setGravity(Gravity.RIGHT);
            edt_length_back.setGravity(Gravity.RIGHT);
            tv_banda_length_value.setGravity(Gravity.RIGHT);
            edt_pocket_width.setGravity(Gravity.RIGHT);
            edt_pocket_length.setGravity(Gravity.RIGHT);
            edt_sleves_width.setGravity(Gravity.RIGHT);
            edt_sleves_length.setGravity(Gravity.RIGHT);
            edt_notes.setGravity(Gravity.RIGHT);
        }


        qalap_radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                final String value = ((RadioButton) findViewById(checkedId)).getText().toString();
                if (value.equals("Visible")) {
                    qalap_visible = "1";
                } else {
                    qalap_visible = "0";
                }
            }
        });
        banda_radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                final String value = ((RadioButton) findViewById(checkedId)).getText().toString();
                if (value.equals("Visible")) {
                    banda_visible = "1";
                } else {
                    banda_visible = "0";
                }
            }
        });

        new getQalapApi("", NewOrderActivity.this, new getQalapApi.OnResultReceived() {
            @Override
            public void onResult(String result) {
                if (AppConstants.thoopList.size() > 0) {
                    if (Preferences.getValue_String(NewOrderActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                        thoop_adapter = new ArrayAdapter<Filter>(NewOrderActivity.this, R.layout.spinner_item, AppConstants.thoopList);
                        spn_thoop_type.setAdapter(thoop_adapter);
                    } else {
                        thoop_adapter = new ArrayAdapter<Filter>(NewOrderActivity.this, R.layout.spinner_item_right, AppConstants.thoopList);
                        spn_thoop_type.setAdapter(thoop_adapter);
                    }
                }
                if (AppConstants.qalapList.size() > 0) {

                    if (Preferences.getValue_String(NewOrderActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                        qalap_adapter = new ArrayAdapter<Filter>(NewOrderActivity.this, R.layout.spinner_item, AppConstants.qalapList);
                        spn_qalap_type.setAdapter(qalap_adapter);
                    } else {
                        qalap_adapter = new ArrayAdapter<Filter>(NewOrderActivity.this, R.layout.spinner_item_right, AppConstants.qalapList);
                        spn_qalap_type.setAdapter(qalap_adapter);
                    }
                }
                if (AppConstants.slevesList.size() > 0) {
                    if (Preferences.getValue_String(NewOrderActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                        sleves_adapter = new ArrayAdapter<Filter>(NewOrderActivity.this, R.layout.spinner_item, AppConstants.slevesList);
                        spn_sleves_type.setAdapter(sleves_adapter);
                    } else {
                        sleves_adapter = new ArrayAdapter<Filter>(NewOrderActivity.this, R.layout.spinner_item_right, AppConstants.slevesList);
                        spn_sleves_type.setAdapter(sleves_adapter);
                    }

                }
                if (AppConstants.bandaList.size() > 0) {

                    if (Preferences.getValue_String(NewOrderActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                        banda_adapter = new ArrayAdapter<Filter>(NewOrderActivity.this, R.layout.spinner_item, AppConstants.bandaList);
                        spn_banda_type.setAdapter(banda_adapter);
                    } else {
                        banda_adapter = new ArrayAdapter<Filter>(NewOrderActivity.this, R.layout.spinner_item_right, AppConstants.bandaList);
                        spn_banda_type.setAdapter(banda_adapter);
                    }
                }


                spn_thoop_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Filter category = (Filter) spn_thoop_type.getSelectedItem();

                        if (category.getId().equals("0")) {

                            thoop_type = "";
                        } else {
                            thoop_type = category.getName();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                spn_qalap_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Filter category = (Filter) spn_qalap_type.getSelectedItem();

                        if (category.getId().equals("0")) {

                            qalap_type = "";
                        } else {
                            qalap_type = category.getName();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spn_sleves_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Filter category = (Filter) spn_sleves_type.getSelectedItem();

                        if (category.getId().equals("0")) {

                            sleves_type = "";
                        } else {
                            sleves_type = category.getName();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spn_banda_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Filter category = (Filter) spn_banda_type.getSelectedItem();

                        if (category.getId().equals("0")) {

                            banda_type = "";
                        } else {
                            banda_type = category.getName();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                setAllData();
            }
        }, true);


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_back.callOnClick();
            }
        });

        btnbooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {


                    Done_Order(NewOrderActivity.this);


                       /* try {
                            AddOrder();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }*/

                       /* try {
                            EditOrder();

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }*/


                }
            }

        });

    }

    private void setBaseWords() {


        tv_thoop_type.setText(AppConstants.baseWords.get("text_lbl_thooptype"));
        lbl_thooptype.setText(AppConstants.baseWords.get("text_lbl_thoop"));
        tv_qalap_type.setText(AppConstants.baseWords.get("qalap_type"));
        lbl_qalaptype.setText(AppConstants.baseWords.get("qalap"));
        tv_length.setText(AppConstants.baseWords.get("length"));
        tv_width.setText(AppConstants.baseWords.get("Width"));
        edt_qalap_length.setHint(AppConstants.baseWords.get("cm"));
        edt_qalap_width.setHint(AppConstants.baseWords.get("cm"));
        tv_chest.setText(AppConstants.baseWords.get("chest"));
        tv_waist.setText(AppConstants.baseWords.get("weist"));
        edt__qalap_chest.setHint(AppConstants.baseWords.get("inches"));
        edt__edt_waist.setHint(AppConstants.baseWords.get("inches"));
        lbl_pockettype.setText(AppConstants.baseWords.get("pocket"));
        tv_pocket_type.setText(AppConstants.baseWords.get("pocket_type"));
        edt_pocket_type.setHint(AppConstants.baseWords.get("pocket_type"));
        tv_pocket_length.setText(AppConstants.baseWords.get("length"));
        tv_pocket_width.setText(AppConstants.baseWords.get("Width"));
        edt_pocket_length.setHint(AppConstants.baseWords.get("cm"));
        edt_pocket_width.setHint(AppConstants.baseWords.get("cm"));
        tv_shoulder.setText(AppConstants.baseWords.get("shoulder"));
        edt_shoulder.setHint(AppConstants.baseWords.get("shoulder"));
        lbl_slevestype.setText(AppConstants.baseWords.get("sleves"));
        tv_sleves_type.setText(AppConstants.baseWords.get("sleves_type"));
        tv_sleves_length.setText(AppConstants.baseWords.get("length"));
        tv_sleves_width.setText(AppConstants.baseWords.get("Width"));
        edt_sleves_length.setHint(AppConstants.baseWords.get("cm"));
        edt_sleves_width.setHint(AppConstants.baseWords.get("cm"));
        tv_length_forward.setText(AppConstants.baseWords.get("length_forward"));
        tv_width_forward.setText(AppConstants.baseWords.get("width_forward"));
        edt_length_forward.setHint(AppConstants.baseWords.get("cm"));
        edt_width_forward.setHint(AppConstants.baseWords.get("cm"));
        tv_length_back.setText(AppConstants.baseWords.get("length_back"));
        edt_length_back.setHint(AppConstants.baseWords.get("cm"));
        tv_banda_type.setText(AppConstants.baseWords.get("banda_type"));
        lbl_bandatype.setText(AppConstants.baseWords.get("banda"));
        tv_banda_length.setText(AppConstants.baseWords.get("banda_length"));
        tv_banda_length_value.setHint(AppConstants.baseWords.get("cm"));
        edt_notes.setHint(AppConstants.baseWords.get("enter_note"));
        btnbooking.setText(AppConstants.baseWords.get("save_order"));
        btn_cancle.setText(AppConstants.baseWords.get("CANCEL"));
        rb__qalap_visible.setText(AppConstants.baseWords.get("visible"));
        rb_qalap_hidden.setText(AppConstants.baseWords.get("hidden"));
        rb_banda_visible.setText(AppConstants.baseWords.get("visible"));
        rb_banda_hidden.setText(AppConstants.baseWords.get("hidden"));

    }

    private void initView() {
        rel_five = (RelativeLayout) findViewById(R.id.rel_five);
        spn_thoop_type = (Spinner) findViewById(R.id.spn_thoop_type);
        spn_qalap_type = (Spinner) findViewById(R.id.spn_qalap_type);
        spn_sleves_type = (Spinner) findViewById(R.id.spn_sleves_type);
        spn_banda_type = (Spinner) findViewById(R.id.spn_banda_type);
        btnbooking = (Button) findViewById(R.id.btnbooking);
        btn_cancle = (Button) findViewById(R.id.btn_cancle);
        img_back = (ImageView) findViewById(R.id.img_back);

        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        lbl_thooptype = (TextView) findViewById(R.id.lbl_thooptype);
        tv_thoop_type = (TextView) findViewById(R.id.tv_thoop_type);
        lbl_qalaptype = (TextView) findViewById(R.id.lbl_qalaptype);
        tv_qalap_type = (TextView) findViewById(R.id.tv_qalap_type);
        tv_length = (TextView) findViewById(R.id.tv_length);
        tv_width = (TextView) findViewById(R.id.tv_width);
        tv_chest = (TextView) findViewById(R.id.tv_chest);
        tv_waist = (TextView) findViewById(R.id.tv_waist);
        lbl_pockettype = (TextView) findViewById(R.id.lbl_pockettype);
        tv_pocket_type = (TextView) findViewById(R.id.tv_pocket_type);
        tv_pocket_length = (TextView) findViewById(R.id.tv_pocket_length);
        tv_pocket_width = (TextView) findViewById(R.id.tv_pocket_width);
        tv_shoulder = (TextView) findViewById(R.id.tv_shoulder);
        lbl_slevestype = (TextView) findViewById(R.id.lbl_slevestype);
        tv_sleves_type = (TextView) findViewById(R.id.tv_sleves_type);
        tv_sleves_length = (TextView) findViewById(R.id.tv_sleves_length);
        tv_sleves_width = (TextView) findViewById(R.id.tv_sleves_width);
        tv_length_forward = (TextView) findViewById(R.id.tv_length_forward);
        tv_width_forward = (TextView) findViewById(R.id.tv_width_forward);
        tv_length_back = (TextView) findViewById(R.id.tv_length_back);
        lbl_bandatype = (TextView) findViewById(R.id.lbl_bandatype);
        tv_banda_type = (TextView) findViewById(R.id.tv_banda_type);
        tv_banda_length = (TextView) findViewById(R.id.tv_banda_length);

        edt_qalap_width = (EditText) findViewById(R.id.edt_qalap_width);
        edt_qalap_length = (EditText) findViewById(R.id.edt_qalap_length);
        edt__qalap_chest = (EditText) findViewById(R.id.edt__qalap_chest);
        edt__edt_waist = (EditText) findViewById(R.id.edt__edt_waist);
        edt_pocket_type = (EditText) findViewById(R.id.edt_pocket_type);
        edt_shoulder = (EditText) findViewById(R.id.edt_shoulder);
        edt_length_forward = (EditText) findViewById(R.id.edt_length_forward);
        edt_width_forward = (EditText) findViewById(R.id.edt_width_forward);
        edt_length_back = (EditText) findViewById(R.id.edt_length_back);
        tv_banda_length_value = (EditText) findViewById(R.id.tv_banda_length_value);
        edt_pocket_width = (EditText) findViewById(R.id.edt_pocket_width);
        edt_pocket_length = (EditText) findViewById(R.id.edt_pocket_length);
        edt_sleves_width = (EditText) findViewById(R.id.edt_sleves_width);
        edt_sleves_length = (EditText) findViewById(R.id.edt_sleves_length);
        edt_notes = (EditText) findViewById(R.id.edt_notes);


        rb__qalap_visible = (RadioButton) findViewById(R.id.rb__qalap_visible);
        rb_qalap_hidden = (RadioButton) findViewById(R.id.rb_qalap_hidden);
        rb_banda_visible = (RadioButton) findViewById(R.id.rb_banda_visible);
        rb_banda_hidden = (RadioButton) findViewById(R.id.rb_banda_hidden);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";

        if (thoop_type.equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_select_thoop_type");
            flagValidate = false;
        } else if (qalap_type.equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_select_qalap_type");
            flagValidate = false;
        } else if (edt_qalap_width.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_qalap_width");
            flagValidate = false;
        } else if (edt_qalap_length.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_qalap_height");
            flagValidate = false;
        } else if (edt__qalap_chest.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_qalap_chest");
            flagValidate = false;
        } else if (edt__edt_waist.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_qalap_weist");
            flagValidate = false;
        } else if (edt_pocket_type.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please enter pocket type");
            flagValidate = false;
        } else if (edt_pocket_width.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_pocket_width");
            flagValidate = false;
        } else if (edt_pocket_length.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_pocket_height");
            flagValidate = false;
        } else if (edt_shoulder.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_shoulder");
            flagValidate = false;
        } else if (sleves_type.equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_select_sleves_type");
            flagValidate = false;
        } else if (edt_sleves_length.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_sleves_height");
            flagValidate = false;
        } else if (edt_sleves_width.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_sleves_width");
            flagValidate = false;
        } else if (edt_length_forward.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_length_forward");
            flagValidate = false;
        } else if (edt_width_forward.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_width_forward");
            flagValidate = false;
        } else if (edt_length_back.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_length_back");
            flagValidate = false;
        } else if (banda_type.equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_select_banda_type");
            flagValidate = false;
        } else if (tv_banda_length_value.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_banda_length");
            flagValidate = false;
        }


        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(NewOrderActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }


    private void setAllData() {


        if (AppConstants.ORDER_ACTION.equals(AppConstants.EDIT_ORDER)) {

            tv_header_title.setText(AppConstants.baseWords.get("edit_order"));
            btnbooking.setText(AppConstants.baseWords.get("save_order"));
            setData();
            setEnable();
            rel_five.setVisibility(View.VISIBLE);
        } else if (AppConstants.ORDER_ACTION.equals(AppConstants.NEW_ORDER)) {
            tv_header_title.setText(AppConstants.baseWords.get("new_Order"));
            btnbooking.setText(AppConstants.baseWords.get("Add_Order"));
            setEnable();
            rel_five.setVisibility(View.VISIBLE);
        } else {
            setData();
            // setEnable();F
            setDisable();
            tv_header_title.setText(AppConstants.orderList.getOrder_name().toUpperCase());
            rel_five.setVisibility(View.GONE);

        }
    }

    private void setData() {
        Log.e("TAG", "Set Data");

        //selectValue(spn_thoop_type, AppConstants.orderList.getThoop_type());
        edt_qalap_width.setText(AppConstants.orderList.getQalap_width());
        edt_qalap_length.setText(AppConstants.orderList.getQalap_length());

        if (AppConstants.orderList.getQalap_h_v().equals("1")) {
            rb__qalap_visible.setChecked(true);
        } else {
            rb_qalap_hidden.setChecked(true);
        }
        qalap_visible = AppConstants.orderList.getQalap_h_v();
        edt__qalap_chest.setText(AppConstants.orderList.getChest());
        edt__edt_waist.setText(AppConstants.orderList.getWaist());
        edt_pocket_type.setText(AppConstants.orderList.getPocket_type());
        edt_pocket_width.setText(AppConstants.orderList.getPocket_width());
        edt_pocket_length.setText(AppConstants.orderList.getPocket_length());
        edt_shoulder.setText(AppConstants.orderList.getShoulder());
        edt_sleves_width.setText(AppConstants.orderList.getSleves_width());
        edt_sleves_length.setText(AppConstants.orderList.getSleves_length());
        edt_length_forward.setText(AppConstants.orderList.getLength_forward());
        edt_width_forward.setText(AppConstants.orderList.getWidth_forward());
        edt_length_back.setText(AppConstants.orderList.getLength_back());
        tv_banda_length_value.setText(AppConstants.orderList.getBanda_length());
        if (AppConstants.orderList.getBanda_h_v().equals("1")) {
            rb_banda_visible.setChecked(true);
        } else {
            rb_banda_hidden.setChecked(true);
        }
        banda_visible = AppConstants.orderList.getBanda_h_v();
        edt_notes.setText(AppConstants.orderList.getMeasur_notes());

        spn_thoop_type.setSelection(getIndex(spn_thoop_type, AppConstants.orderList.getThoop_type()));
        spn_qalap_type.setSelection(getIndex(spn_qalap_type, AppConstants.orderList.getQalap_type()));
        spn_sleves_type.setSelection(getIndex(spn_sleves_type, AppConstants.orderList.getSleves_type()));
        spn_banda_type.setSelection(getIndex(spn_banda_type, AppConstants.orderList.getBanda_type()));

    }

    private int getIndex(Spinner spinner, String myString) {
        int index = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            Log.e("TAG", "spinner Index: " + spinner.getItemAtPosition(i));
            if (spinner.getItemAtPosition(i).toString().equals(myString)) {
                index = i;
            }
        }
        return index;
    }

    private void AddOrder(String Amount) {
        try {
            //String param = "&user_id=" + ""/*Preferences.getValue_String(NewOrderActivity.this, Preferences.USER_ID)*/;
            String param = "&booking_id=" + AppConstants.booking_id;
            param = param + "&qalap_type=" + qalap_type;
            param = param + "&qalap_length=" + edt_qalap_length.getText().toString().trim();
            param = param + "&qalap_width=" + edt_qalap_width.getText().toString().trim();
            param = param + "&qalap_h_v=" + qalap_visible;
            param = param + "&thoop_type=" + thoop_type;
            param = param + "&chest=" + edt__qalap_chest.getText().toString().trim();
            param = param + "&waist=" + edt__edt_waist.getText().toString().trim();
            param = param + "&pocket_type=" + URLEncoder.encode(edt_pocket_type.getText().toString().trim(), AppConstants.encodeType);
            param = param + "&pocket_length=" + edt_pocket_length.getText().toString().trim();
            param = param + "&pocket_width=" + edt_pocket_width.getText().toString().trim();
            param = param + "&shoulder=" + URLEncoder.encode(edt_shoulder.getText().toString().trim(), AppConstants.encodeType);
            param = param + "&sleves_type=" + sleves_type;
            param = param + "&sleves_length=" + edt_sleves_length.getText().toString().trim();
            param = param + "&sleves_width=" + edt_sleves_width.getText().toString().trim();
            param = param + "&length_forward=" + edt_length_forward.getText().toString().trim();
            param = param + "&width_forward=" + edt_width_forward.getText().toString().trim();
            param = param + "&length_back=" + edt_length_back.getText().toString().trim();
            param = param + "&banda_type=" + banda_type;
            param = param + "&banda_length=" + tv_banda_length_value.getText().toString().trim();
            param = param + "&banda_h_v=" + banda_visible;
            param = param + "&measur_notes=" + URLEncoder.encode(edt_notes.getText().toString().trim(), AppConstants.encodeType);
            param = param + "&amount=" + Amount;

            new NewOrderApi(param, NewOrderActivity.this, new NewOrderApi.OnResultReceived() {
                @Override
                public void onResult(String result) {
                    AppConstants.IsBookingDetails = true;
                }
            }, true);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.e(TAG, "Error : " + e.getMessage());
        }
    }

    private void EditOrder(String Amount) {
        try {
            //String param = "&user_id=" + ""/*Preferences.getValue_String(NewOrderActivity.this, Preferences.USER_ID)*/;
            String param = "&booking_id=" + AppConstants.booking_id;
            param = param + "&measurement_id=" + AppConstants.orderList.getMeasurement_id();
            param = param + "&qalap_type=" + qalap_type;
            param = param + "&qalap_length=" + edt_qalap_length.getText().toString().trim();
            param = param + "&qalap_width=" + edt_qalap_width.getText().toString().trim();
            param = param + "&qalap_h_v=" + qalap_visible;
            param = param + "&thoop_type=" + thoop_type;
            param = param + "&chest=" + edt__qalap_chest.getText().toString().trim();
            param = param + "&waist=" + edt__edt_waist.getText().toString().trim();
            param = param + "&pocket_type=" + URLEncoder.encode(edt_pocket_type.getText().toString().trim(), AppConstants.encodeType);
            param = param + "&pocket_length=" + edt_pocket_length.getText().toString().trim();
            param = param + "&pocket_width=" + edt_pocket_width.getText().toString().trim();
            param = param + "&shoulder=" + URLEncoder.encode(edt_shoulder.getText().toString().trim(), AppConstants.encodeType);
            param = param + "&sleves_type=" + sleves_type;
            param = param + "&sleves_length=" + edt_sleves_length.getText().toString().trim();
            param = param + "&sleves_width=" + edt_sleves_width.getText().toString().trim();
            param = param + "&length_forward=" + edt_length_forward.getText().toString().trim();
            param = param + "&width_forward=" + edt_width_forward.getText().toString().trim();
            param = param + "&length_back=" + edt_length_back.getText().toString().trim();
            param = param + "&banda_type=" + banda_type;
            param = param + "&banda_length=" + tv_banda_length_value.getText().toString().trim();
            param = param + "&banda_h_v=" + banda_visible;
            param = param + "&measur_notes=" + URLEncoder.encode(edt_notes.getText().toString().trim(), AppConstants.encodeType);
            param = param + "&amount=" + Amount;
            param = param + "&order_id=" + AppConstants.orderList.getOrder_id();

            new EditOrderApi(param, NewOrderActivity.this, new EditOrderApi.OnResultReceived() {
                @Override
                public void onResult(String result) {
                    AppConstants.IsBookingDetails = true;
                }
            }, true);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.e(TAG, "Error : " + e.getMessage());
        }
    }

    public void setEnable() {
        spn_thoop_type.setEnabled(true);
        spn_qalap_type.setEnabled(true);
        spn_banda_type.setEnabled(true);
        edt_qalap_width.setEnabled(true);
        edt_qalap_length.setEnabled(true);
        edt__qalap_chest.setEnabled(true);
        edt__edt_waist.setEnabled(true);
        edt_pocket_type.setEnabled(true);
        edt_shoulder.setEnabled(true);
        edt_length_forward.setEnabled(true);
        edt_width_forward.setEnabled(true);
        edt_length_back.setEnabled(true);
        tv_banda_length_value.setEnabled(true);
        edt_pocket_width.setEnabled(true);
        edt_pocket_length.setEnabled(true);
        edt_sleves_width.setEnabled(true);
        edt_sleves_length.setEnabled(true);
        edt_notes.setEnabled(true);
        rb__qalap_visible.setEnabled(true);
        rb_qalap_hidden.setEnabled(true);
        rb_banda_visible.setEnabled(true);
        rb_banda_hidden.setEnabled(true);

    }

    public void setDisable() {
        spn_thoop_type.setEnabled(false);
        spn_qalap_type.setEnabled(false);
        spn_banda_type.setEnabled(false);
        edt_qalap_width.setEnabled(false);
        edt_qalap_length.setEnabled(false);
        edt__qalap_chest.setEnabled(false);
        edt__edt_waist.setEnabled(false);
        edt_pocket_type.setEnabled(false);
        edt_shoulder.setEnabled(false);
        edt_length_forward.setEnabled(false);
        edt_width_forward.setEnabled(false);
        edt_length_back.setEnabled(false);
        tv_banda_length_value.setEnabled(false);
        edt_pocket_width.setEnabled(false);
        edt_pocket_length.setEnabled(false);
        edt_sleves_width.setEnabled(false);
        edt_sleves_length.setEnabled(false);
        edt_notes.setEnabled(false);


        if (AppConstants.orderList.getQalap_h_v().equals("1")) {
            rb__qalap_visible.setChecked(true);
            rb_qalap_hidden.setEnabled(false);
        } else {
            rb_qalap_hidden.setChecked(true);
            rb__qalap_visible.setEnabled(false);
        }


        if (AppConstants.orderList.getBanda_h_v().equals("1")) {
            rb_banda_visible.setChecked(true);
            rb_banda_hidden.setEnabled(false);
        } else {
            rb_banda_hidden.setChecked(true);
            rb_banda_visible.setChecked(false);
        }
    }


    private void Done_Order(final Context mContext) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_price, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        final TextView tv_dialog_title = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
        final EditText edt_amount = (EditText) dialogView.findViewById(R.id.edt_amount);
        Log.e(TAG, AppConstants.EDIT_ORDER);
        if (AppConstants.ORDER_ACTION.equals(AppConstants.EDIT_ORDER)) {
            edt_amount.setText(AppConstants.orderList.getAmount());
            Log.e(TAG, "Amount :" + AppConstants.orderList.getAmount());
        } else {
            edt_amount.setText("");
        }

        final EditText edt_new_pass = (EditText) dialogView.findViewById(R.id.edt_new_pass);
        final EditText edt_confirm_pass = (EditText) dialogView.findViewById(R.id.edt_confirm_pass);
        edt_new_pass.setVisibility(View.GONE);
        edt_confirm_pass.setVisibility(View.GONE);
        final TextView tv_save = (TextView) dialogView.findViewById(R.id.tv_save);
        final TextView tv_cancel = (TextView) dialogView.findViewById(R.id.tv_cancel);


        tv_dialog_title.setText(AppConstants.baseWords.get("Done_Order"));
        tv_save.setText(AppConstants.baseWords.get("text_btn_done"));
        tv_cancel.setText(AppConstants.baseWords.get("CANCEL"));


        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean flagValidate = true;
                String validateMsg = "";
                if (edt_amount.getText().toString().trim().equals("")) {
                    validateMsg = "Please enter price";
                    flagValidate = false;
                } else {
                    flagValidate = true;
                }

                if (flagValidate) {
                    b.dismiss();
                    if (AppConstants.ORDER_ACTION.equals(AppConstants.NEW_ORDER)) {
                        AddOrder(edt_amount.getText().toString());
                        try {
                            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), NewOrderActivity.this)) {
                                String envelope = "&mobile=966553437652";
                                envelope = envelope + "&password=thoop200";
                                envelope = envelope + "&numbers=" + AppConstants.country_code + AppConstants.bookingDetails.getPhoneno();
                                envelope = envelope + "&sender=Thoop";


                                if (Preferences.getValue_String(NewOrderActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                                    envelope = envelope + "&msg=" + getString(R.string.Thank) + getString(R.string.you) + getString(R.string.For) + getString(R.string.using) + getString(R.string.Thoop) + getString(R.string.application) + getString(R.string.coma) +
                                            getString(R.string.your) + getString(R.string.order) + getString(R.string.has) + getString(R.string.been) + getString(R.string.received) + getString(R.string.coma) + getString(R.string.and) +
                                            getString(R.string.it) + getString(R.string.is) + getString(R.string.being) + getString(R.string.executed);
                                } else {
                                    envelope = envelope + "&msg=" + getString(R.string.Thank_you_for_using_Thoop_application_Your_order_has_been_received_and_it_is_being_executed);
                                }



                                /*next line : 00000085 */                                                 /*space : 00000020 */

                                envelope = envelope + "&timeSend=0";
                                envelope = envelope + "&dateSend=0";
                                envelope = envelope + "&applicationType=68";
                                envelope = envelope + "&domainName=demo.Thoopksa.com";
                                envelope = envelope + "&msgId=";

                                new SMSApi(envelope, NewOrderActivity.this, new SMSApi.OnResultReceived() {
                                    @Override
                                    public void onResult(String result) {
                                        //AppConstants.ACTION = AppConstants.CHANGE_MOBILE;
                                    }
                                }, true);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        EditOrder(edt_amount.getText().toString());

                    }

                   /* if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), NewOrderActivity.this)) {
                        String param = "&order_id=" + Preferences.getValue_String(mContext, Preferences.USER_ID);
                        param = param + "&booking_id=" + AppConstants.booking_id;
                        param = param + "&amount=" + edt_old_pass.getText().toString().trim();
                        param = param + "&done_order=1";

                        new DoneBookingApi(param, mContext, new DoneBookingApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) {
                                AppConstants.IshistoryBooking = true;
                                AppConstants.IscurrentBooking = true;
                            }
                        }, true);

                    }*/
                } else {
                    DialogManager.errorDialog(mContext, "Error!", validateMsg);
                }


            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    public String Unicode(String s) {
        String value = "";
        for (int i = 0; i < s.length(); i++) {

            if (String.valueOf(s.charAt(i)).equals("1")) {
                value = value + "00000031";
            } else if (String.valueOf(s.charAt(i)).equals("2")) {
                value = value + "00000032";
            } else if (String.valueOf(s.charAt(i)).equals("3")) {
                value = value + "00000033";
            } else if (String.valueOf(s.charAt(i)).equals("4")) {
                value = value + "00000034";
            } else if (String.valueOf(s.charAt(i)).equals("5")) {
                value = value + "00000035";
            } else if (String.valueOf(s.charAt(i)).equals("6")) {
                value = value + "00000036";
            } else if (String.valueOf(s.charAt(i)).equals("7")) {
                value = value + "00000037";
            } else if (String.valueOf(s.charAt(i)).equals("8")) {
                value = value + "00000038";
            } else if (String.valueOf(s.charAt(i)).equals("9")) {
                value = value + "00000039";
            } else if (String.valueOf(s.charAt(i)).equals("0")) {
                value = value + "00000030";
            }
        }
        return value;

    }


}