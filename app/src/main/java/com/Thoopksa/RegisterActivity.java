package com.Thoopksa;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.Thoopksa.api.EmailValidateApi;
import com.Thoopksa.api.SMSApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.Preferences;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class RegisterActivity extends AppCompatActivity {
    private String TAG = getClass().getSimpleName();
    EditText edt_fullname, edt_mobile, edt_email, edt_password, edt_confirmpassword;
    Button btnRegister;
    public static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0;
    Spinner spn_countrycode;
    ImageView logo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
        setIcon();
        setBaseWords();
        send();

        if (Preferences.getValue_String(RegisterActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            edt_email.setGravity(Gravity.LEFT);
            edt_password.setGravity(Gravity.LEFT);
            edt_fullname.setGravity(Gravity.LEFT);
            edt_mobile.setGravity(Gravity.LEFT);
            edt_confirmpassword.setGravity(Gravity.LEFT);
//            spn_countrycode.setGravity(Gravity.LEFT);

        } else {
            edt_email.setGravity(Gravity.RIGHT);
            edt_password.setGravity(Gravity.RIGHT);
            edt_fullname.setGravity(Gravity.RIGHT);
            edt_mobile.setGravity(Gravity.RIGHT);
            edt_confirmpassword.setGravity(Gravity.RIGHT);
          //  spn_countrycode.setGravity(Gravity.RIGHT);
        }
        String[] country = new String[]{
                "966",
                "91",
                "965",
                "974",
                "971"
        };
       /* ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_items, country
        );*/

       /* if (Preferences.getValue_String(RegisterActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_items);
            spn_countrycode.setAdapter(spinnerArrayAdapter);
        } else {
            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_items_right);
            spn_countrycode.setAdapter(spinnerArrayAdapter);
        }*/


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isValidate()) {
                            setdata();
                            validateEmail();
                            //  sendmessage();
                        }
                    }
                }, 300);
            }
        });

        edt_confirmpassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                }
                return false;
            }
        });
    }
    private void setIcon() {
        if (Preferences.getValue_String(RegisterActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            logo.setImageResource(R.drawable.app_logo_english);
        } else {
            logo.setImageResource(R.drawable.app_logo);
        }
    }

    private void validateEmail() {
        String envelope = "&email=" + edt_email.getText().toString();
        // String envelope = "&email="+"pratik.shah@Thoopksa.com";
        new EmailValidateApi(envelope, RegisterActivity.this, new EmailValidateApi.OnResultReceived() {
            @Override
            public void OnResult(String result) {
                sendmessage();
            }
        }, true);
    }

    private void setBaseWords() {
        edt_fullname.setHint(AppConstants.baseWords.get("text_hint_full_name"));
        edt_email.setHint(AppConstants.baseWords.get("text_hint_email"));
        edt_mobile.setHint(AppConstants.baseWords.get("text_hint_mobile"));
        edt_password.setHint(AppConstants.baseWords.get("text_hint_password"));
        edt_confirmpassword.setHint(AppConstants.baseWords.get("text_hint_confirm_password"));
        btnRegister.setText(AppConstants.baseWords.get("text_btn_register"));
    }

    private int getRandomNumber(int min, int max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }

    protected void send() {
        Log.e(TAG, "send");
        if (Build.VERSION.SDK_INT >= 23) {
            Log.e(TAG, "sdk");
            if (checkSelfPermission(android.Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "self permission");
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.SEND_SMS)) {
                    Log.e(TAG, "should show permission");
                    String message = "You need to grant SEND SMS permission to send sms";
                    showMessageOkCancle(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= 23) {
                                        requestPermissions(new String[]{android.Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);
                                    }
                                }
                            });
                    return;
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);
                    return;
                }
            }
            Log.e(TAG, "sdk");
        }
        Log.e(TAG, "send");
    }

    public void sendmessage() {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), RegisterActivity.this)) {
            String envelope = "&mobile=966553437652";
            envelope = envelope + "&password=thoop200";
            envelope = envelope + "&numbers="+"966"+edt_mobile.getText().toString();
           // Log.e(TAG, "Country Code : " + spn_countrycode.getSelectedItem());
            Log.e(TAG, "Mobile number : " + edt_mobile.getText().toString());

            envelope = envelope + "&sender=Thoop";


            envelope = envelope + "&msg=" + getString(R.string.Thank) + getString(R.string.you) + getString(R.string.For) + getString(R.string.Registering) + getString(R.string.in) + getString(R.string.Thoop) + getString(R.string.Application)
                    + getString(R.string.please) + getString(R.string.enter) + getString(R.string.the) + getString(R.string.number) + getString((R.string.in)) + getString(R.string.the) + getString(R.string.application)
                    + getString(R.string.verification) + getString(R.string.number) + Unicode(AppConstants.verification_code);


            envelope = envelope + "&timeSend=0";
            envelope = envelope + "&dateSend=0";
            envelope = envelope + "&applicationType=68";
            envelope = envelope + "&domainName=demo.Thoopksa.com";
            envelope = envelope + "&msgId=";

            new SMSApi(envelope, RegisterActivity.this, new SMSApi.OnResultReceived() {
                @Override
                public void onResult(String result) {
                    AppConstants.ACTION = AppConstants.REGISTER;
                }
            }, true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();

                perms.put(Manifest.permission.SEND_SMS, PackageManager.PERMISSION_GRANTED);


                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {


                } else {


                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void setdata() {
        AppConstants.code_fullname = edt_fullname.getText().toString();
        AppConstants.code_email = edt_email.getText().toString();
        AppConstants.code_mobile = "966"+ edt_mobile.getText().toString();
        AppConstants.code_password = edt_password.getText().toString();
        AppConstants.code_confirmpassword = edt_confirmpassword.getText().toString();
        AppConstants.verification_code = String.valueOf(getRandomNumber(1111, 9999));

    }


    private void initView() {
        edt_mobile = (EditText) findViewById(R.id.edt_mobile);
        edt_fullname = (EditText) findViewById(R.id.edt_fullname);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);
        edt_confirmpassword = (EditText) findViewById(R.id.edt_confirmpassword);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        logo = (ImageView) findViewById(R.id.logo);
        //spn_countrycode = (Spinner) findViewById(R.id.spn_countrycode);
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";


        if (edt_fullname.getText().toString().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_fullname");
            flagValidate = false;
        } else if (edt_email.getText().toString().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_email");
            flagValidate = false;
        } else if (!AppConstants.validateEmail(edt_email.getText().toString().trim())) {
            validateMsg = AppConstants.baseWords.get("Please_enter_valid_email");
            flagValidate = false;
        } else if (edt_mobile.getText().toString().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_mobile_number");
            flagValidate = false;
        } else if (!edt_mobile.getText().toString().startsWith("5")) {
            validateMsg = AppConstants.baseWords.get("Phone_number__should_start_with_5");
            flagValidate = false;
        } else if (!(edt_mobile.getText().toString().length() == 9)) {
            validateMsg = AppConstants.baseWords.get("Please_enter_valid_9_digit_phone_number");
            flagValidate = false;
        }else if (edt_password.getText().toString().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_password");
            flagValidate = false;
        } else if (edt_confirmpassword.getText().toString().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_confirm_password");
            flagValidate = false;
        } else if (!edt_confirmpassword.getText().toString().equals(edt_password.getText().toString())) {
            validateMsg = AppConstants.baseWords.get("Not_match_confirm_password");
            flagValidate = false;
        }

        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(RegisterActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }

    private void showMessageOkCancle(String message, DialogInterface.OnClickListener okListener) {

        new android.app.AlertDialog.Builder(RegisterActivity.this)
                .setMessage(message)
                .setPositiveButton(AppConstants.baseWords.get("OK"), okListener)
                .setNegativeButton(AppConstants.baseWords.get("Cancle"), null)
                .create()
                .show();
    }


    public String Unicode(String s) {
        String value = "";
        for (int i = 0; i < s.length(); i++) {

            if (String.valueOf(s.charAt(i)).equals("1")) {
                value = value + "00000031";
            } else if (String.valueOf(s.charAt(i)).equals("2")) {
                value = value + "00000032";
            } else if (String.valueOf(s.charAt(i)).equals("3")) {
                value = value + "00000033";
            } else if (String.valueOf(s.charAt(i)).equals("4")) {
                value = value + "00000034";
            } else if (String.valueOf(s.charAt(i)).equals("5")) {
                value = value + "00000035";
            } else if (String.valueOf(s.charAt(i)).equals("6")) {
                value = value + "00000036";
            } else if (String.valueOf(s.charAt(i)).equals("7")) {
                value = value + "00000037";
            } else if (String.valueOf(s.charAt(i)).equals("8")) {
                value = value + "00000038";
            } else if (String.valueOf(s.charAt(i)).equals("9")) {
                value = value + "00000039";
            } else if (String.valueOf(s.charAt(i)).equals("0")) {
                value = value + "00000030";
            }
        }
        return value;

    }
}

