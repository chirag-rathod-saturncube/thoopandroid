package com.Thoopksa;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Thoopksa.adapter.BookingDetailsAdapter;
import com.Thoopksa.api.BookingDetailsApi;
import com.Thoopksa.api.CopyBookingApi;
import com.Thoopksa.api.GetDateTimmingApi;
import com.Thoopksa.api.NewBookingApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.Preferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


@RequiresApi(api = Build.VERSION_CODES.N)
public class NewBookingActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    String TAG = getClass().getSimpleName();
    RecyclerView recycler_view;
    LinearLayoutManager linearLayoutManager;
    RelativeLayout rel_second, rel_time, rel_date, relStarttime, relEndtime;
    TextView tv_booking_name_value, tv_start_time_value, tv_end_time_value, tv_header_title, tv_address, tv_select_location_value, tv_select_location;
    ImageView img_back, imageView1;
    TextView tv_select_date, tv_booking_name, tv_start_time, tv_end_time, tv_select_location1, tv_select_date1;
    Button btnbooking, btn_cancle;
    String strAdd = "";

    String select_date = "";
    String select_start_time = "";
    String select_end_time = "";

    GoogleMap map;

    boolean flagZoom = true;
    private LatLng center;
    SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mma");
    //SimpleDateFormat displayFormat = new SimpleDateFormat("kk:mm");

    LocationRequest mLocationRequest;
    Location mLastLocation;
    PendingResult<LocationSettingsResult> result;
    private GoogleApiClient mGoogleApiClient;
    final static int REQUEST_LOCATION = 199;
    private Handler mUiHandler = new Handler();
    int delay = 2000;

    final private int REQUEST_CODE_ASK_LOCATION_PERMISSIONS = 124;
    boolean flagSetLocation = true;
    BookingDetailsAdapter adapter;
    Calendar calendar;
    Calendar calendar_end;
    int min_Hour;
    int min_Minute;
    int min_Second = 00;

    int max_Hour;
    int max_Minute;
    int max_Second = 00;

    String date;

    //Google places integrate:

    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

    String lattitude, longitude = "";

    RelativeLayout rel_booking;

    String slected_date = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (Locale.getDefault().getDisplayLanguage().equals("English")){*/
        if (Preferences.getValue_String(NewBookingActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            Log.e(TAG, "if : " + "txt_eng");
            setContentView(R.layout.activity_new_booking);
        } else {
            Log.e(TAG, "else : " + "txt_arb");
            setContentView(R.layout.activity_new_booking_arabic);
        }
        getDateTime();
       /* }else {
            if (Preferences.getValue_String(NewBookingActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                Log.e(TAG, "if : " + "txt_eng");
                setContentView(R.layout.activity_new_booking);
            } else {
                Log.e(TAG, "else : " + "txt_arb");
                setContentView(R.layout.activity_new_booking_arabic);
            }
        }*/


        initView();
        setBaseWords();
        tv_booking_name_value.setText(Preferences.getValue_String(NewBookingActivity.this, Preferences.FULL_NAME) + "-" + Preferences.getValue_String(NewBookingActivity.this, Preferences.PHONE_NO));
        Log.e(TAG, AppConstants.viewprofile.getFullname() + "-" + AppConstants.viewprofile.getPhoneno());

        if (Preferences.getValue_String(NewBookingActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            tv_booking_name_value.setGravity(Gravity.LEFT);
            tv_address.setGravity(Gravity.LEFT);
            tv_select_location_value.setGravity(Gravity.LEFT);
            tv_start_time_value.setGravity(Gravity.LEFT);
            tv_end_time_value.setGravity(Gravity.LEFT);
            tv_select_date.setGravity(Gravity.LEFT);
        } else {
            tv_booking_name_value.setGravity(Gravity.RIGHT);
            tv_address.setGravity(Gravity.RIGHT);
            tv_select_location_value.setGravity(Gravity.RIGHT);
            tv_start_time_value.setGravity(Gravity.RIGHT);
            tv_end_time_value.setGravity(Gravity.RIGHT);
            tv_select_date.setGravity(Gravity.RIGHT);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        buildGoogleApiClient();
        getLocationPermission();

        btnbooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (AppConstants.ACTION.equals(AppConstants.NEW_BOOKING)) {

                    if (select_date.equals("")) {
                        DialogManager.errorDialog(NewBookingActivity.this, "Required", AppConstants.baseWords.get("Please_select_date"));

                    } else if (select_start_time.equals("")) {
                        DialogManager.errorDialog(NewBookingActivity.this, "Required", AppConstants.baseWords.get("Please_select_start_time"));

                    } else if (select_end_time.equals("")) {
                        DialogManager.errorDialog(NewBookingActivity.this, "Required", AppConstants.baseWords.get("Please_select_end_time"));

                    } else if (tv_start_time_value.getText().toString().equals(tv_end_time_value.getText().toString())) {
                        DialogManager.errorDialog(NewBookingActivity.this, "Required", AppConstants.baseWords.get("text_Validationforsametime"));
                    } else if (select_start_time.equals(select_end_time)) {
                        DialogManager.errorDialog(NewBookingActivity.this, "Required", AppConstants.baseWords.get("text_Validationforsametime"));
                    } else {


                        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), NewBookingActivity.this)) {
                            try {
                                //String envelope = "&user_id=" + ""/*Preferences.getValue_String(NewBookingActivity.this, Preferences.USER_ID)*/;
                                String envelope = "&booking_date=" + URLEncoder.encode(select_date, AppConstants.encodeType);
                                envelope = envelope + "&booking_start_time=" + URLEncoder.encode(select_start_time, AppConstants.encodeType);
                                envelope = envelope + "&booking_end_time=" + URLEncoder.encode(select_end_time, AppConstants.encodeType);
                                envelope = envelope + "&latitude=" + lattitude;
                                envelope = envelope + "&longitude=" + longitude;
                                envelope = envelope + "&address=" + URLEncoder.encode(strAdd, AppConstants.encodeType);

                                new NewBookingApi(envelope, NewBookingActivity.this, new NewBookingApi.OnResultReceived() {
                                    @Override
                                    public void onResult(String result) {
                                        AppConstants.IscurrentBooking = true;
                                        finish();
                                    }
                                }, true);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                  /*  if (select_date.equals("")) {
                        DialogManager.errorDialog(NewBookingActivity.this, "Required", "Please select date");

                    } else if (select_start_time.equals("")) {
                        DialogManager.errorDialog(NewBookingActivity.this, "Required", "Please select start time");

                    } else if (select_end_time.equals("")) {
                        DialogManager.errorDialog(NewBookingActivity.this, "Required", "Please select end time");

                    } else {*/
                    String ids = "";
                    if (adapter != null) {
                        ids = adapter.getOrderIds();
                        if (!ids.equals("")) {
                            ids = ids.substring(0, ids.length() - 1);
                        }
                    } else {
                        ids = "";
                    }
                    if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), NewBookingActivity.this)) {
                        // String envelope = "&user_id=" + ""/*Preferences.getValue_String(NewBookingActivity.this, Preferences.USER_ID)*/;
                        String envelope = null;
                        try {
                            //envelope = envelope + "&booking_date=" +URLEncoder.encode(tv_select_date.getText().toString().trim(),AppConstants.encodeType);
                            envelope = "&booking_date=" + URLEncoder.encode(AppConstants.bookingDetails.getBooking_date(), AppConstants.encodeType);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        envelope = envelope + "&booking_id=" + AppConstants.booking_id;
                        envelope = envelope + "&booking_start_time=" + AppConstants.bookingDetails.getBooking_start_time();
                        envelope = envelope + "&booking_end_time=" + AppConstants.bookingDetails.getBooking_end_time();
                        envelope = envelope + "&latitude=" + lattitude;
                        envelope = envelope + "&longitude=" + longitude;
                        envelope = envelope + "&order_id=" + ids;
                        try {
                            envelope = envelope + "&address=" + URLEncoder.encode(strAdd, AppConstants.encodeType);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        new CopyBookingApi(envelope, NewBookingActivity.this, new CopyBookingApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) {
                                AppConstants.IscurrentBooking = true;


                            }

                        }, true);
                        finish();
                    }


                    // }
                }
            }
        });
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                startActivity(new Intent(NewBookingActivity.this, HomeActivity.class));
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                dialog.dismiss();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(NewBookingActivity.this);
                builder.setMessage(AppConstants.baseWords.get("Are_you_sure_you_want_to_cancel_booking?"))
                        .setPositiveButton(AppConstants.baseWords.get("Yes"), dialogClickListener)
                        .setNegativeButton(AppConstants.baseWords.get("No"), dialogClickListener)
                        .show();
            }
        });
        tv_select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* DatePickerDialog select_date = new DatePickerDialog(NewBookingActivity.this, R.style.DialogTheme, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));

                select_date.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                select_date.show();*/


                DatePickerDialog datePickerDialog = new DatePickerDialog().newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        String myFormat = "dd-MM-yyyy";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

//                        tv_select_date.setText(AppConstants.FormatDate(sdf.format(calendar.getTime())));
//                        tv_select_date.setText(AppConstants.arabicToDecimal(AppConstants.FormatDate(sdf.format(calendar.getTime()))));
                        tv_select_date.setText(AppConstants.arabicToDecimal((sdf.format(calendar.getTime()))));

                        String ApiFormat = "yyyy-MM-dd";
                        SimpleDateFormat formater = new SimpleDateFormat(ApiFormat, Locale.ENGLISH);
                        select_date = formater.format(calendar.getTime()).toString();
                        // rel_booking.setVisibility(View.VISIBLE);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                try {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                    datePickerDialog.setMinDate(calendar);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                Log.e(TAG, "calendar : " + calendar);
                //datePickerDialog.setMinDate();
                datePickerDialog.show(NewBookingActivity.this.getFragmentManager(), "DatepickerdialogFrom");
            }
        });

           /* DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    String myFormat = "yyyy-MM-dd";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

                    tv_select_date.setText(AppConstants.FormatDate(sdf.format(calendar.getTime())));
                    select_date = sdf.format(calendar.getTime()).toString();
                    rel_booking.setVisibility(View.VISIBLE);
                    tv_booking_name_value.setText("Booking-" + select_date);
                }
            };
        });*/
        tv_start_time_value.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                final TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                                                                                           @Override
                                                                                           public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                                                                                               calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                                                                               calendar.set(Calendar.MINUTE, minute);
                                                                                               tv_start_time_value.setText(displayFormat.format(calendar.getTime()));

                                                                                               SimpleDateFormat formater = new SimpleDateFormat("hh:mma", Locale.ENGLISH);
                                                                                               select_start_time = formater.format(calendar.getTime());
                                                                                           }
                                                                                       },
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE), false);
//                timePickerDialog.setMaxTime(23, 59, 59);
//                timePickerDialog.setMinTime(8, 00, 00);
                timePickerDialog.setMaxTime(max_Hour, max_Minute, 00);
                timePickerDialog.setMinTime(min_Hour, min_Minute, 00);
                timePickerDialog.show(NewBookingActivity.this.getFragmentManager(), "DatepickerdialogFrom");


            }
        });
        tv_end_time_value.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                                                                                     @Override
                                                                                     public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                                                                                         calendar_end.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                                                                         calendar_end.set(Calendar.MINUTE, minute);
                                                                                         Log.e("End time", "" + calendar_end.getTime());
                                                                                         tv_end_time_value.setText(displayFormat.format(calendar_end.getTime()));

                                                                                         SimpleDateFormat formater = new SimpleDateFormat("hh:mma", Locale.ENGLISH);
                                                                                         select_end_time = formater.format(calendar_end.getTime());
                                                                                     }
                                                                                 },
                        calendar_end.get(Calendar.HOUR_OF_DAY),
                        calendar_end.get(Calendar.MINUTE), false);
//                timePickerDialog.setMaxTime(23, 59, 59);
//                timePickerDialog.setMinTime(8, 00, 00);
                timePickerDialog.setMaxTime(max_Hour, max_Minute, 00);
                timePickerDialog.setMinTime(min_Hour, min_Minute, 00);
                timePickerDialog.show(NewBookingActivity.this.getFragmentManager(), "DatepickerdialogFrom");


            }
        });
    }

    private void setBaseWords() {
        tv_booking_name.setText(AppConstants.baseWords.get("text_lbl_booking_name"));
        tv_booking_name_value.setHint(AppConstants.baseWords.get("text_lbl_booking_name"));
        tv_select_date1.setText(AppConstants.baseWords.get("text_lbl_date"));
        tv_select_date.setHint(AppConstants.baseWords.get("text_lbl_date"));
        tv_start_time.setText(AppConstants.baseWords.get("text_lbl_start_time"));
        tv_start_time_value.setHint(AppConstants.baseWords.get("text_lbl_start_time"));
        tv_end_time.setText(AppConstants.baseWords.get("text_lbl_end_time"));
        tv_end_time_value.setHint(AppConstants.baseWords.get("text_lbl_end_time"));
        tv_select_location1.setText(AppConstants.baseWords.get("Select_location_on_map"));
        btnbooking.setText(AppConstants.baseWords.get("CONFIRM_BOOKING"));
        btn_cancle.setText(AppConstants.baseWords.get("CANCEL"));
        tv_select_location.setText(AppConstants.baseWords.get("text_lbl_address"));
        tv_header_title.setText(AppConstants.baseWords.get("text_NEW_BOOKING"));

    }

    private void initView() {
        // mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        // mapFragment.getMapAsync(NewBookingActivity.this);
        rel_booking = (RelativeLayout) findViewById(R.id.rel_booking);
        btnbooking = (Button) findViewById(R.id.btnbooking);
        btn_cancle = (Button) findViewById(R.id.btn_cancle);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(NewBookingActivity.this);
        recycler_view.setLayoutManager(linearLayoutManager);
        rel_second = (RelativeLayout) findViewById(R.id.rel_second);
        rel_time = (RelativeLayout) findViewById(R.id.rel_time);
        rel_date = (RelativeLayout) findViewById(R.id.rel_date);
        relStarttime = (RelativeLayout) findViewById(R.id.relStarttime);
        relEndtime = (RelativeLayout) findViewById(R.id.relEndtime);

        tv_select_date = (TextView) findViewById(R.id.tv_select_date);
        tv_select_date1 = (TextView) findViewById(R.id.tv_select_date1);
        tv_booking_name = (TextView) findViewById(R.id.tv_booking_name);
        tv_start_time = (TextView) findViewById(R.id.tv_start_time);
        tv_end_time = (TextView) findViewById(R.id.tv_end_time);
        tv_select_location1 = (TextView) findViewById(R.id.tv_select_location1);
        tv_select_location = (TextView) findViewById(R.id.tv_select_location);
        tv_select_location_value = (TextView) findViewById(R.id.tv_select_location_value);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_booking_name_value = (TextView) findViewById(R.id.tv_booking_name_value);
        tv_start_time_value = (TextView) findViewById(R.id.tv_start_time_value);
        tv_end_time_value = (TextView) findViewById(R.id.tv_end_time_value);
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        tv_header_title.setText("NEW BOOKING".toUpperCase());

        img_back = (ImageView) findViewById(R.id.img_back);
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_cancle.callOnClick();
            }
        });
        calendar = Calendar.getInstance();
        calendar_end = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar_end.setTimeInMillis(System.currentTimeMillis());
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (AppConstants.ACTION.equals(AppConstants.NEW_BOOKING)) {
            tv_header_title.setText(AppConstants.baseWords.get("text_NEW_BOOKING"));
            recycler_view.setVisibility(View.GONE);
            rel_second.setVisibility(View.GONE);
            rel_booking.setVisibility(View.VISIBLE);

        } else {
            rel_booking.setVisibility(View.VISIBLE);
            tv_header_title.setText(AppConstants.baseWords.get("text_lbl_copybookings"));
            rel_date.setVisibility(View.GONE);
            rel_time.setVisibility(View.GONE);


            String param = "&booking_id=" + AppConstants.booking_id;

            new BookingDetailsApi(param, NewBookingActivity.this, new BookingDetailsApi.OnResultReceived() {
                @Override
                public void onResult(String result) {


                    setData();
                    Log.e("TAG", "SIZE-->" + AppConstants.orderlistArraylist.size());

                    if (AppConstants.orderlistArraylist.size() > 0) {

                        strAdd = AppConstants.bookingDetails.getAddress();
                        recycler_view.setVisibility(View.VISIBLE);
                        rel_second.setVisibility(View.VISIBLE);
                        adapter = new BookingDetailsAdapter(NewBookingActivity.this, AppConstants.orderlistArraylist);
                        recycler_view.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        recycler_view.setVisibility(View.GONE);
                        rel_second.setVisibility(View.GONE);
                    }

                }
            }, true);
        }
    }

    private void getDateTime() {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), NewBookingActivity.this)) {
            new GetDateTimmingApi(this, new GetDateTimmingApi.OnResultReceived() {
                @Override
                public void onResult(String result) {
                    min_Hour = Integer.parseInt(AppConstants.getDateTime.getStart_hours());
                    min_Minute = Integer.parseInt(AppConstants.getDateTime.getStart_minutes());
                    max_Hour = Integer.parseInt(AppConstants.getDateTime.getEnd_hours());
                    max_Minute = Integer.parseInt(AppConstants.getDateTime.getEnd_minutes());
                    date = AppConstants.getDateTime.getMin_date();

                    Log.e(TAG, "min_Hour : " + AppConstants.getDateTime.getStart_hours());
                    Log.e(TAG, "min_Minute : " + AppConstants.getDateTime.getStart_minutes());
                    Log.e(TAG, "max_Hour : " + AppConstants.getDateTime.getEnd_hours());
                    Log.e(TAG, "max_Minute : " + AppConstants.getDateTime.getEnd_minutes());
                    Log.e(TAG, "date : " + AppConstants.getDateTime.getMin_date());
                }
            }, true);
        }
    }

    private void setData() {
        tv_booking_name_value.setText(AppConstants.bookingDetails.getBooking_name());
    }

    public void getLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            int hasCameraPermission = checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showMessageOKCancel("You need to allow access to Location",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= 23)
                                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                                REQUEST_CODE_ASK_LOCATION_PERMISSIONS);
                                }
                            });
                    return;
                }
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_LOCATION_PERMISSIONS);
                return;
            }
            enableGPSDialog();
        } else {
            enableGPSDialog();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        Log.e(TAG, "buildGoogleApiClient(){..}");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        Log.e(TAG, "onMapReady(){..}");
        map = googleMap;
        locationButtonEnable();


        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                center = googleMap.getCameraPosition().target;
                lattitude = "" + center.latitude;
                longitude = "" + center.longitude;
                getCompleteAddressString(center, false);

                /*LocationAddress.getAddressFromLocation(center.latitude
                        , center.longitude
                        , NewBookingActivity.this
                        , new GeocoderHandler());*/

            }
        });

    }

//    private class GeocoderHandler extends Handler {
//        @Override
//        public void handleMessage(Message message) {
//            String locationAddress;
//            switch (message.what) {
//                case 1:
//                    Bundle bundle = message.getData();
//                    locationAddress = bundle.getString("address");
//                    break;
//                default:
//                    locationAddress = null;
//            }
//            Log.e(TAG, "Address from LatLong: " + locationAddress);
//            tv_address.setText(locationAddress);
//        }
//    }

    @Override
    public void onBackPressed() {
        btn_cancle.callOnClick();
    }

    private void setUpMap() {
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setTiltGesturesEnabled(true);
        map.getUiSettings().setRotateGesturesEnabled(true);
        map.getUiSettings().setScrollGesturesEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setZoomGesturesEnabled(true);

        /*Log.e(TAG, "Selected Map Type: " + Preferences.getValue_String(SelectLocationActivity.this, Preferences.mapType));
        if (Preferences.getValue_String(SelectLocationActivity.this, Preferences.mapType).equals(AppConstants.MAP_NORMAL)) {
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else if (Preferences.getValue_String(SelectLocationActivity.this, Preferences.mapType).equals(AppConstants.MAP_TERRAIN)) {
            map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        } else {
            map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        }*/

    }

    public void locationButtonEnable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                map.setMyLocationEnabled(true);
                setUpMap();
            }
        } else {
            map.setMyLocationEnabled(true);
            setUpMap();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        Toast.makeText(NewBookingActivity.this, "Location enabled", Toast.LENGTH_LONG).show();
                        mUiHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG, "Ready to work with GPS");
                                //requestLocationUpdate();
                            }
                        }, delay);
                        delay += 2000;
                        break;
                    }

                    case Activity.RESULT_CANCELED: {
                        Toast.makeText(NewBookingActivity.this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        break;
                    }

                    default: {
                        break;
                    }
                }
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_LOCATION_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show();
                    mUiHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            enableGPSDialog();
                        }
                    }, delay);
                    delay += 2000;

                } else {
                    // Permission Denied
                    Toast.makeText(NewBookingActivity.this, "Location Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "onConnected(){..}");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(200);
        if (ContextCompat.checkSelfPermission(NewBookingActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            enableGPSDialog();
        }
    }

    public void enableGPSDialog() {
        Log.e(TAG, "enableGPSDialog(){..}");
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.e(TAG, "Ready to work with GPS");
                        locationButtonEnable();
                        requestLocationUpdate();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(NewBookingActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });

    }

    public void requestLocationUpdate() {
        Log.e(TAG, "requestLocationUpdate(){..}");
        if (ContextCompat.checkSelfPermission(NewBookingActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, NewBookingActivity.this);
        } else {
            Log.e(TAG, "Permission not granted: ");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        //Log.e(TAG, "onLocationChanged(){..}");
        //Log.e(TAG, "Lat: " + location.getLatitude() + " Lng: " + location.getLongitude());


        lattitude = "" + location.getLatitude();
        longitude = "" + location.getLongitude();
        if (AppConstants.map_lat.equals("") && AppConstants.map_lng.equals("")) {
            mLastLocation = location;
            if (flagSetLocation) {
                if (mLastLocation != null) {
                    if (map != null) {
                        map.clear();
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        if (flagZoom) {
                            map.animateCamera(CameraUpdateFactory.zoomTo(12));
                            flagZoom = false;
                        }
                    }
                }
                flagSetLocation = false;
            }
        } else {

            Log.e(TAG, "Select Map Lat: " + AppConstants.map_lat);
            Log.e(TAG, "Select Map Lng: " + AppConstants.map_lng);

            mLastLocation = location;
            if (flagSetLocation) {
                if (mLastLocation != null) {
                    if (map != null) {
                        map.clear();
                        LatLng latLng = new LatLng(Double.parseDouble(AppConstants.map_lat),
                                Double.parseDouble(AppConstants.map_lng));
                        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        if (flagZoom) {
                            map.animateCamera(CameraUpdateFactory.zoomTo(12));
                            flagZoom = false;
                        }
                    }
                }
                flagSetLocation = false;
            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(NewBookingActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    //    if you get any address regarding problem please refer below solution,i think problem is in the 4.4 and 7.0 device
//    Rebooting the Device will solve the problem
    private String getCompleteAddressString(LatLng center, boolean isClose) {
        Log.e(TAG, "getCompleteAddressString(){...}");

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        Log.e(TAG, "geocoder : " + geocoder);
        Log.e(TAG, "latitude : " + center.latitude);
        Log.e(TAG, "longitude : " + center.longitude);
        try {
            Log.e(TAG, "geocoder" + geocoder.getFromLocation(center.latitude, center.longitude, 1));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            List<Address> addresses = geocoder.getFromLocation(center.latitude, center.longitude, 1);
            if (addresses != null) {
                Log.e(TAG, " if (addresses != null)");
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = returnedAddress.getAddressLine(0);
                Log.e(TAG, "My Current location addresses: " + addresses);
                Log.e(TAG, "My Current location : " + returnedAddress.getAddressLine(0));
                Log.e(TAG, "My Current location address: " + strAdd);
                // Toast.makeText(this, "My Current location address: " + strAdd, Toast.LENGTH_SHORT).show();

                tv_select_location_value.setText(strAdd);
                AppConstants.map_address = strAdd;
                getCountryName(NewBookingActivity.this, center.latitude, center.longitude, isClose);
            } else {
                Log.e(TAG, "else " + " My Current location address No Address returned! ");
                // Toast.makeText(this, "My Current location address No Address returned!", Toast.LENGTH_SHORT).show();
                tv_select_location_value.setText("" + strAdd);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "catch " + "My Current location address No Address returned! " + e.getMessage());
            //Toast.makeText(this, "My Current location address No Address returned!", Toast.LENGTH_SHORT).show();
            tv_select_location_value.setText("" + strAdd);
        }
        return strAdd;
    }


    public void getCountryName(Context context, double latitude, double longitude, boolean isClose) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Address result;
            if (addresses != null && !addresses.isEmpty()) {
                Log.e(TAG, "Country: " + addresses.get(0).getCountryName());
                AppConstants.map_country = addresses.get(0).getCountryName();
                if (isClose) {
                    finish();
                }
            } else {
                Log.e(TAG, "Country not found");
                Toast.makeText(context, "Country: " + addresses.get(0).getCountryName(), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException ignored) {
            //do something
        }
    }


    //Google places api implement here
   /* private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            hideSoftKeyboard();
            String address = mAutocompleteView.getText().toString();
            Log.e(TAG, "Address: " + address);
            String[] parts = address.split(",");
            Log.e(TAG, "Country: " + parts[parts.length - 1]);
            getLocationFromAddress(address);

            AppConstants.post_location = address;
            AppConstants.post_country = parts[parts.length - 1].toLowerCase().trim();
            //finish();
        }
    };


    public void getLocationFromAddress(String strAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address != null) {
                Address location = address.get(0);
                if (map != null) {
                    map.clear();
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    map.animateCamera(CameraUpdateFactory.zoomTo(12));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }*/

}
