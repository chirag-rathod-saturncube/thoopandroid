package com.Thoopksa.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.LoadingDialog;
import com.Thoopksa.common.Preferences;
import com.Thoopksa.common.WebServices;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class GetDateTimmingApi {
    private String url = WebServices.GET_DATE_TIMMING;
    String TAG = getClass().getSimpleName();

    Context mContext;
    private OnResultReceived onResultReceived;
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;

    public GetDateTimmingApi(Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public interface OnResultReceived {
        public void onResult(String result) ;
    }


    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }


    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            String status = response.optString("success");
                            Log.e("response :  ", String.valueOf(response));
                            if (status.equals("0")) {
                                hide();
                                Log.e(TAG, "status.equals(\"0\")");
                                //DialogManager.errorDialog(mContext, "Error!", response.optString("msg"));
                                Toast.makeText(mContext, response.optString("msg"), Toast.LENGTH_SHORT).show();
                            } else if (status.equals("1")) {
                                Log.e(TAG, "status.equals(\"1\")");
                                JSONObject result = response.getJSONObject("result");

                                AppConstants.getDateTime.setSetting_id(result.optString("setting_id"));
                                AppConstants.getDateTime.setMin_date(result.optString("min_date"));
                                AppConstants.getDateTime.setStart_time(result.optString("start_time"));
                                AppConstants.getDateTime.setEnd_time(result.optString("end_time"));
                                AppConstants.getDateTime.setStart_hours(result.optString("start_hours"));
                                AppConstants.getDateTime.setStart_minutes(result.optString("start_minutes"));
                                AppConstants.getDateTime.setEnd_hours(result.optString("end_hours"));
                                AppConstants.getDateTime.setEnd_minutes(result.optString("end_minutes"));


                                hide();
                                if (onResultReceived != null) {
                                    onResultReceived.onResult(response.toString());
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, AppConstants.baseWords.get("Something_is_wrong_with_api"), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                params.put("Auth", Preferences.getValue_String(mContext, Preferences.USER_JWT));
                return params;
            }
        };
        queue.add(postRequest);
    }
}
