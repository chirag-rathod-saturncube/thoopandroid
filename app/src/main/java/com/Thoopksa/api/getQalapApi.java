package com.Thoopksa.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.LoadingDialog;
import com.Thoopksa.common.Preferences;
import com.Thoopksa.common.WebServices;
import com.Thoopksa.moduls.Filter;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class getQalapApi {
    private String url = WebServices.GET_QALAP;
    String TAG = getClass().getSimpleName();

    Context mContext;
    private OnResultReceived onResultReceived;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;

    public getQalapApi(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public interface OnResultReceived {
        public void onResult(String result) ;
    }


    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }


    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            String status = response.optString("success");
                            Log.e("response :  ", String.valueOf(response));
                            if (status.equals("0")) {
                                hide();
                                //DialogManager.errorDialog(mContext, "Error!", response.optString("msg"));
                                Toast.makeText(mContext, response.optString("msg"), Toast.LENGTH_SHORT).show();
                            } else if (status.equals("1")) {
                                JSONArray cut = response.getJSONArray("qalap");
                                AppConstants.qalapList.clear();
                                if (cut.length() > 0) {


                                    AppConstants.qalapList.add(new Filter("0","--"+AppConstants.baseWords.get("select_qalap")+"--","qalap"));

                                    for (int i = 0; i < cut.length(); i++) {
                                        JSONObject object = cut.getJSONObject(i);
                                        Filter filter = new Filter();
                                        filter.setId(object.optString("qalap_id"));
                                        filter.setName(object.optString("qalap_name"));
                                        filter.setCode(object.optString("code"));

                                        AppConstants.qalapList.add(filter);
                                    }
                                }



                                JSONArray color = response.getJSONArray("banda");
                                AppConstants.bandaList.clear();
                                if (color.length() > 0) {
                                    AppConstants.bandaList.add(new Filter("0","--"+AppConstants.baseWords.get("select_banda")+"--","banda"));
                                    for (int i = 0; i < color.length(); i++) {
                                        JSONObject object = color.getJSONObject(i);
                                        Filter filter = new Filter();
                                        filter.setId(object.optString("banda_id"));
                                        filter.setName(object.optString("banda_name"));
                                        filter.setCode(object.optString("code"));

                                        AppConstants.bandaList.add(filter);
                                    }
                                }

                                JSONArray clarity = response.getJSONArray("sleves");
                                AppConstants.slevesList.clear();
                                if (clarity.length() > 0) {
                                    AppConstants.slevesList.add(new Filter("0","--"+AppConstants.baseWords.get("select_sleves")+"--","sleves"));
                                    for (int i = 0; i < clarity.length(); i++) {
                                        JSONObject object = clarity.getJSONObject(i);
                                        Filter filter = new Filter();
                                        filter.setId(object.optString("sleves_id"));
                                        filter.setName(object.optString("sleves_name"));
                                        filter.setCode(object.optString("code"));

                                        AppConstants.slevesList.add(filter);
                                    }
                                }

                                JSONArray carat = response.getJSONArray("thoop");
                                AppConstants.thoopList.clear();
                                if (carat.length() > 0) {
                                    AppConstants.thoopList.add(new Filter("0","--"+AppConstants.baseWords.get("select_thoop")+"--","thoop"));
                                    for (int i = 0; i < carat.length(); i++) {
                                        JSONObject object = carat.getJSONObject(i);
                                        Filter filter = new Filter();
                                        filter.setId(object.optString("thoop_id"));
                                        filter.setName(object.optString("thoop_name"));
                                        filter.setCode(object.optString("code"));
                                        AppConstants.thoopList.add(filter);
                                    }
                                }

                                hide();
                                if (onResultReceived != null) {
                                    onResultReceived.onResult(response.toString());
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, AppConstants.baseWords.get("Something_is_wrong_with_api"), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                params.put("Auth", Preferences.getValue_String(mContext, Preferences.USER_JWT));
                return params;
            }
        };
        queue.add(postRequest);
    }
}
