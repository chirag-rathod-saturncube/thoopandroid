package com.Thoopksa.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.LoadingDialog;
import com.Thoopksa.common.Preferences;
import com.Thoopksa.common.WebServices;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Saturncube-5 on 5/1/2017.
 */

public class LogoutApi {

    private String url = WebServices.LOGOUT_API;
    String TAG = getClass().getSimpleName();

    Context mContext;
    private OnResultReceived onResultReceived;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;

    public LogoutApi(String envelope, Context mContext, OnResultReceived onResultRecieved, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultRecieved;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();

    }

    public interface OnResultReceived {
        public void OnResult(String result) ;
    }

    public void show() {
        if (flagProgress) {

            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }

    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            if (response.has("success")) {
                                if (response.getString("success").equals("0")) {
                                    hide();
                                    DialogManager.errorDialog(mContext, "Error!", response.optString("msg"));
                                    Log.e(TAG, "success 0 " + "msg : " + response.optString("msg"));
                                    //Toast.makeText(mContext, response.optString("msg"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Log.e(TAG, "success 1 " + "msg : " + response.optString("msg"));
                                    //DialogManager.successDialog(mContext, "Success!", response.optString("msg"));
                                    // Toast.makeText(mContext, response.optString("msg"), Toast.LENGTH_SHORT).show();
                                    hide();
                                    if (onResultReceived != null) {
                                        onResultReceived.OnResult(response.toString());
                                    }
                                }


                            } else {
                                hide();
                                Toast.makeText(mContext, AppConstants.baseWords.get("Something_is_wrong_with_api"), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + "Error:...." + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");

                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-type", "application/json");
                params.put("Cache-Control", "no-cache");
                params.put("Auth", Preferences.getValue_String(mContext, Preferences.USER_JWT));
                return params;
            }
        };
        queue.add(postRequest);
    }
}

