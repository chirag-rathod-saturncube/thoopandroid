package com.Thoopksa.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.Thoopksa.VerificationActivity;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.LoadingDialog;
import com.Thoopksa.common.WebServices;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Saturncube-5 on 5/1/2017.
 */

public class GenerateOTPApi {

    private String response = "";
    private String url = WebServices.GENERATE_OTP_API;
    String TAG = getClass().getSimpleName();

    Context mContext;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;
    private OnResultReceived onResultReceived;


    public GenerateOTPApi(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public interface OnResultReceived {
        public void onResult(String result);
    }

    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }

    public OnResultReceived getOnResultReceived() {
        return onResultReceived;
    }

    private void getResponse() {
        show();
        Log.e(TAG, "Url : " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String success = response.optString("success");
                            Log.e("response :  ", String.valueOf(response));
                            if (success.equals("0")) {
                                hide();
                                //DialogManager.errorDialog(mContext, "Error!", response.optString("msg"));
                                Toast.makeText(mContext, response.optString("msg"), Toast.LENGTH_SHORT).show();
                            } else if (success.equals("1")) {
                                hide();

                                Intent intent = new Intent(mContext, VerificationActivity.class);
                                mContext.startActivity(intent);
                                ((Activity) mContext).finish();

                                // DialogManager.errorDialog(mContext, "Success...",  response.optString("msg"));
                                //Toast.makeText(mContext, response.optString("msg"), Toast.LENGTH_SHORT).show();
                                if (onResultReceived != null) {
                                    onResultReceived.onResult(response.toString());
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, AppConstants.baseWords.get("Something_is_wrong_with_api"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + "Error:..." + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                params.put("Auth", WebServices.COMMON_JWT);
                return params;
            }
        };
        queue.add(postRequest);
    }
}