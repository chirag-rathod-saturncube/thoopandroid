package com.Thoopksa.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.LoadingDialog;
import com.Thoopksa.common.Preferences;
import com.Thoopksa.common.WebServices;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class EmployeeLoginApi {
    private String response = "";
    private String url = WebServices.EMPLOYEE_LOGIN_URL;
    String TAG = getClass().getSimpleName();

    Context mContext;
    private OnResultReceived onResultReceived;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;


    public EmployeeLoginApi(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public interface OnResultReceived {
        public void onResult(String result) ;
    }

    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }


    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            String status = response.optString("success");
                            Log.e("response :  ", String.valueOf(response));
                            if (status.equals("0")) {
                                hide();
                                // DialogManager.errorDialog(mContext, "Error!",  response.optString("msg"));
                                Toast.makeText(mContext, response.optString("msg"), Toast.LENGTH_SHORT).show();
                            } else if (status.equals("1")) {
                                JSONObject object = response.getJSONObject("result");

                                Preferences.setValue(mContext, Preferences.FULL_NAME, object.optString("fullname"));
                                Preferences.setValue(mContext, Preferences.ROLE_ID, object.optString("role_id"));
                                Preferences.setValue(mContext, Preferences.EMAIL, object.optString("email"));
                                Preferences.setValue(mContext, Preferences.USER_ID, object.optString("user_id"));

                                // Preferences.setValue(mContext, Preferences.PASSWORD, object.optString("password"));
                                //Preferences.setValue(mContext, Preferences.PHONE_NO, object.optString("phoneno"));
                                // Preferences.setValue(mContext, Preferences.USER_PIC, response.optString("profile_img"));
                                AppConstants.IscurrentBooking = true;
                                hide();
                                if (onResultReceived != null) {
                                    onResultReceived.onResult(response.toString());
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, AppConstants.baseWords.get("Something_is_wrong_with_api"), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                params.put("Auth", WebServices.COMMON_JWT);
                return params;
            }
        };
        queue.add(postRequest);
    }
}
