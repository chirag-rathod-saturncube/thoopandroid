package com.Thoopksa.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.LoadingDialog;
import com.Thoopksa.common.Preferences;
import com.Thoopksa.common.WebServices;
import com.Thoopksa.moduls.OrderList;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class BookingDetailsApi {
    private String url = WebServices.BOOKING_DETAILS_API;
    String TAG = getClass().getSimpleName();

    Context mContext;
    private OnResultReceived onResultReceived;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;

    public BookingDetailsApi(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public interface OnResultReceived {
        public void onResult(String result) ;
    }


    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }


    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            String status = response.optString("success");
                            Log.e("response :  ", String.valueOf(response));
                            if (status.equals("0")) {
                                hide();
                               // DialogManager.errorDialog(mContext, "Error!", response.optString("msg"));
                                Toast.makeText(mContext,response.optString("msg"), Toast.LENGTH_SHORT).show();
                            } else if (status.equals("1")) {
                                JSONObject result = response.getJSONObject("result");


                                AppConstants.bookingDetails.setBooking_name(result.optString("booking_name"));
                                AppConstants.bookingDetails.setBooking_date(result.optString("booking_date"));
                                AppConstants.bookingDetails.setBooking_start_time(result.optString("booking_start_time"));
                                AppConstants.bookingDetails.setBooking_end_time(result.optString("booking_end_time"));
                                AppConstants.bookingDetails.setLatitude(result.optString("latitude"));
                                AppConstants.bookingDetails.setLongitude(result.optString("longitude"));
                                AppConstants.bookingDetails.setAddress(result.optString("address"));
                                AppConstants.bookingDetails.setStatus_booking(result.optString("status_booking"));
                                AppConstants.bookingDetails.setPhoneno(result.optString("phoneno"));
                                AppConstants.bookingDetails.setAmount(result.optString("amount"));


                                JSONArray object1 = result.getJSONArray("orderList");

                                AppConstants.orderlistArraylist.clear();

                                if (object1.length() > 0) {

                                    for (int i = 0; i < object1.length(); i++) {
                                        JSONObject orderList = object1.getJSONObject(i);
                                        OrderList orderList1 = new OrderList();
                                        orderList1.setBooking_name(orderList.optString("booking_name"));
                                        orderList1.setBooking_id(orderList.optString("booking_id"));
                                        orderList1.setUser_id(orderList.optString("user_id"));
                                        orderList1.setBooking_date(orderList.optString("booking_date"));
                                        orderList1.setBooking_start_time(orderList.optString("booking_start_time"));
                                        orderList1.setBooking_end_time(orderList.optString("booking_end_time"));
                                        orderList1.setLatitude(orderList.optString("latitude"));
                                        orderList1.setLongitude(orderList.optString("longitude"));
                                        orderList1.setAddress(orderList.optString("address"));
                                        orderList1.setOrder_id(orderList.optString("order_id"));
                                        orderList1.setComplete_booking(orderList.optString("complete_booking"));
                                        orderList1.setCrt_date(orderList.optString("crt_date"));
                                        orderList1.setNew_update_date(orderList.optString("new_update_date"));
                                        orderList1.setActive_flag(orderList.optString("active_flag"));
                                        orderList1.setMeasurement_id(orderList.optString("measurement_id"));
                                        orderList1.setOrder_name(orderList.optString("order_name"));
                                        orderList1.setAmount(orderList.optString("amount"));
                                        orderList1.setFinish_order(orderList.optString("finish_order"));
                                        orderList1.setDone_order(orderList.optString("done_order"));
                                        orderList1.setQalap_type(orderList.optString("qalap_type"));
                                        orderList1.setQalap_length(orderList.optString("qalap_length"));
                                        orderList1.setQalap_width(orderList.optString("qalap_width"));
                                        orderList1.setQalap_h_v(orderList.optString("qalap_h_v"));
                                        orderList1.setThoop_type(orderList.optString("thoop_type"));
                                        orderList1.setChest(orderList.optString("chest"));
                                        orderList1.setWaist(orderList.optString("waist"));
                                        orderList1.setPocket_type(orderList.optString("pocket_type"));
                                        orderList1.setPocket_length(orderList.optString("pocket_length"));
                                        orderList1.setPocket_width(orderList.optString("pocket_width"));
                                        orderList1.setShoulder(orderList.optString("shoulder"));
                                        orderList1.setSleves_type(orderList.optString("sleves_type"));
                                        orderList1.setSleves_length(orderList.optString("sleves_length"));
                                        orderList1.setSleves_width(orderList.optString("sleves_width"));
                                        orderList1.setLength_forward(orderList.optString("length_forward"));
                                        orderList1.setWidth_forward(orderList.optString("width_forward"));
                                        orderList1.setLength_back(orderList.optString("length_back"));
                                        orderList1.setBanda_type(orderList.optString("banda_type"));
                                        orderList1.setBanda_length(orderList.optString("banda_length"));
                                        orderList1.setBanda_h_v(orderList.optString("banda_h_v"));
                                        orderList1.setMeasur_notes(orderList.optString("measur_notes"));
                                        orderList1.setAmount(orderList.optString("amount"));
                                        AppConstants.orderlistArraylist.add(orderList1);
                                    }
                                }
                                hide();
                                if (onResultReceived != null) {
                                    onResultReceived.onResult(response.toString());
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext,  AppConstants.baseWords.get("Something_is_wrong_with_api"), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                params.put("Auth", Preferences.getValue_String(mContext, Preferences.USER_JWT));
                return params;
            }
        };
        queue.add(postRequest);
    }
}
