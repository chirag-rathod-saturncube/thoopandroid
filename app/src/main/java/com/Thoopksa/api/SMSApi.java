package com.Thoopksa.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.Thoopksa.VerificationActivity;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.LoadingDialog;
import com.Thoopksa.common.WebServices;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class SMSApi {
    private String url = "http://www.mobily.ws/api/msgSend.php?";
    String TAG = getClass().getSimpleName();

    Context mContext;
    private OnResultReceived onResultReceived;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;

    public SMSApi(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public interface OnResultReceived {
        public void onResult(String result) ;
    }

    public void show() {
        if (flagProgress) {

            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }


    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        StringRequest postRequest = new StringRequest(com.android.volley.Request.Method.GET, url + envelope,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            if (response.equals("1")) {
                                if (AppConstants.ACTION.equals(AppConstants.LOGIN)) {
                                    Intent intent = new Intent(mContext, VerificationActivity.class);
                                    mContext.startActivity(intent);
                                    ((Activity) mContext).finish();
                                }
                                hide();
                                if (onResultReceived != null) {
                                    onResultReceived.onResult(response.toString());
                                }
                            } else if (response.equals("2")) {
                                hide();
                                Toast.makeText(mContext, "Your balance is 0", Toast.LENGTH_SHORT).show();

                            } else if (response.equals("3")) {
                                hide();
                                Toast.makeText(mContext, "Your balance is not enough", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("4")) {
                                hide();
                                Toast.makeText(mContext, "Invalid mobile number (or invalid username)", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("5")) {
                                hide();
                                Toast.makeText(mContext, " Invalid password", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("6")) {
                                hide();
                                Toast.makeText(mContext, "SMS-API not responding, please try again", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("10")) {
                                hide();
                                Toast.makeText(mContext, "SMS counts don’t match mobiles numbers count", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("13")) {
                                hide();
                                Toast.makeText(mContext, "Sender name is not accepted", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("14")) {
                                hide();
                                Toast.makeText(mContext, "Sender name is not active from Mobily.ws and mobile telecommunications companies.", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("15")) {
                                hide();
                                Toast.makeText(mContext, "Mobile(s) number(s) is not specified or incorrect", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("16")) {
                                hide();
                                Toast.makeText(mContext, "Sender name is not specified", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("17")) {
                                hide();
                                Toast.makeText(mContext, "Message text is not specified or not encoded properly with Mobily.ws Unicode", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("18")) {
                                hide();
                                Toast.makeText(mContext, "Sending SMS stopped from support", Toast.LENGTH_SHORT).show();
                            } else if (response.equals("19")) {
                                hide();
                                Toast.makeText(mContext, "applicationType is not specified or invalid", Toast.LENGTH_SHORT).show();
                            } else {
                                hide();
                                Toast.makeText(mContext, AppConstants.baseWords.get("Something_is_wrong_with_api"), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        hide();
                        VolleyLog.d("Error.Response", "");
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                params.put("Auth", WebServices.COMMON_JWT);
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }
}
