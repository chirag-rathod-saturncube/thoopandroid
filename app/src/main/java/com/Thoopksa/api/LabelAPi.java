package com.Thoopksa.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.LoadingDialog;
import com.Thoopksa.common.WebServices;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pc4 on 7/6/2016.
 */
public class LabelAPi {

    private String url = WebServices.LABEL_API;
    String TAG = getClass().getSimpleName();

    Context mContext;
    private OnResultReceived onResultReceived;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;

    public LabelAPi(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public interface OnResultReceived {
        public void onResult(String result) ;
    }


    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();

        }
    }


    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            if (response.has("error")) {
                                hide();
                                DialogManager.errorDialog(mContext, "Error!", response.optString("error"));
                            } else if (response.has("success")) {
                                JSONObject object = response.getJSONObject("result");

                                AppConstants.baseWords.clear();
                                for (int i = 0; i < object.length(); i++) {
                                    String values = object.names().getString(i);
                                    if (!object.get(object.names().getString(i)).equals("")) {
                                        values = object.get(object.names().getString(i)).toString();
                                    }
                                    AppConstants.baseWords.put(object.names().getString(i), values);
                                }

                                HashMap<String, String> sortedMap = sortByKeys(AppConstants.baseWords);
                                Set set2 = sortedMap.entrySet();
                                Iterator iterator2 = set2.iterator();
                                while (iterator2.hasNext()) {
                                    Map.Entry me2 = (Map.Entry) iterator2.next();
                                    Log.e(TAG, "keys: " + me2.getKey() + " => " + me2.getValue());
                                }

                                hide();
                                if (onResultReceived != null) {
                                    onResultReceived.onResult(response.toString());
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, AppConstants.baseWords.get("Something_is_wrong_with_api"), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                params.put("Auth", WebServices.COMMON_JWT);
                return params;
            }
        };
        queue.add(postRequest);
    }

    private static HashMap sortByKeys(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getKey()).compareTo(((Map.Entry) (o2)).getKey());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
}
