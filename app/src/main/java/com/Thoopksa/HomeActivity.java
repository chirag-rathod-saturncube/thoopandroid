package com.Thoopksa;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.Preferences;


public class HomeActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Button btnOrder;
    ImageView tv_logout, img_about, logo;
    String name[] = {AppConstants.baseWords.get("CURRENT_BOOKINGS"), AppConstants.baseWords.get("BOOKING_HISTORY")};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Log.e(TAG, "JWT : " + Preferences.getValue_String(HomeActivity.this, Preferences.USER_JWT));
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tv_logout = (ImageView) findViewById(R.id.tv_logout);
        img_about = (ImageView) findViewById(R.id.img_about);
        logo = (ImageView) findViewById(R.id.logo);
        btnOrder = (Button) findViewById(R.id.btnOrder);
        setIcon();
        setBaseWords();

        if (Preferences.getValue_String(HomeActivity.this, Preferences.ROLE_ID).equals(AppConstants.USER_ROLE)) {
            btnOrder.setVisibility(View.VISIBLE);
        } else {
            btnOrder.setVisibility(View.GONE);
        }

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ACTION = AppConstants.NEW_BOOKING;
                startActivity(new Intent(HomeActivity.this, NewBookingActivity.class));
            }
        });

       /* tabLayout.addTab(tabLayout.newTab().setText("CURRENT BOOKINGS"));
        tabLayout.addTab(tabLayout.newTab().setText("BOOKING HISTORY"));*/
        tabLayout.addTab(tabLayout.newTab().setText(AppConstants.baseWords.get("CURRENT_BOOKINGS")));
        tabLayout.addTab(tabLayout.newTab().setText(AppConstants.baseWords.get("BOOKING_HISTORY")));
        tabLayout.setTabGravity(tabLayout.GRAVITY_FILL);

        Pager adapter = new Pager(getSupportFragmentManager(), name);
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ColorStateList.valueOf(Color.WHITE));

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(HomeActivity.this, SettingsActivity.class));

            }
        });
        img_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.contactUsDialog(HomeActivity.this, AppConstants.baseWords.get("Contact_Us"), AppConstants.baseWords.get("Please_mail_to:-") /*+ "\n Support@thoopks.com"*/ /*"\n support@thoop.com"*/);
            }
        });
    }

    private void setIcon() {
        if (Preferences.getValue_String(HomeActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            logo.setImageResource(R.drawable.app_logo_english);
        } else {
            logo.setImageResource(R.drawable.app_logo);
        }
    }

    private void setBaseWords() {
        tabLayout.addTab(tabLayout.newTab().setText(AppConstants.baseWords.get("CURRENT_BOOKINGS")));
        tabLayout.addTab(tabLayout.newTab().setText(AppConstants.baseWords.get("BOOKING_HISTORY")));
        btnOrder.setText(AppConstants.baseWords.get("Order"));
    }

    @Override
    protected void onResume() {
        super.onResume();

        tabLayout.getTabAt(0).setText(AppConstants.baseWords.get("CURRENT_BOOKINGS"));
        tabLayout.getTabAt(1).setText(AppConstants.baseWords.get("BOOKING_HISTORY"));
        tabLayout.setTabGravity(tabLayout.GRAVITY_FILL);
        btnOrder.setText(AppConstants.baseWords.get("Order"));
    }
}
