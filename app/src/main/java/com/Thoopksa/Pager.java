package com.Thoopksa;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.Thoopksa.Fragment.BookingHistoryFmt;


/**
 * Created by Saturncube-5 on 4/24/2017.
 */

class Pager extends FragmentStatePagerAdapter {
    String name[];

    public Pager(FragmentManager fm,String[] strings) {
        super(fm);
        this.name=strings;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                com.Thoopksa.Fragment.CurrentBookingFmt currentBooking=new com.Thoopksa.Fragment.CurrentBookingFmt();
                return currentBooking;
            case 1:
                BookingHistoryFmt bookingHistory=new BookingHistoryFmt();
                return bookingHistory;
            default:
                return null;

        }

    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return name[position];
    }
}
