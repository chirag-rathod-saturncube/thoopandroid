package com.Thoopksa;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Thoopksa.adapter.BookingDetailsAdapter;
import com.Thoopksa.api.BookingDetailsApi;
import com.Thoopksa.api.CreateArabicpdfApi;
import com.Thoopksa.api.CreatePdfApi;
import com.Thoopksa.api.DoneBookingApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.Preferences;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.Map;


public class BookingDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    ImageView img_back, img_option;
    TextView tv_header_title, tv_booking_name_value, tv_select_date_value, tv_start_time_value, tv_end_time_value, tv_address_value;
    TextView tv_booking_name, tv_select_date, tv_start_time, tv_end_time, tv_address, tv_orders, tv_addorder;

    String TAG = getClass().getSimpleName();


    RecyclerView recycler_view;
    LinearLayoutManager linearLayoutManager;
    BookingDetailsAdapter adapter;


    RelativeLayout rel_five, rel_view, rel_map;
    Button btnbooking, btn_cancle;
    RelativeLayout rel_addorder;


    Double latitude1;
    Double longitude1;
    SupportMapFragment mapFragment;
    TextView tv_done;
    ImageView img_download;
    String phoneno;
    String latitude;
    String longitude;
    String addresss;

    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Preferences.getValue_String(BookingDetailsActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            setContentView(R.layout.activity_booking_details);
        } else {
            setContentView(R.layout.activity_booking_details_arabic);
        }
        initView();
        setBaseWords();

        if (Preferences.getValue_String(BookingDetailsActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            tv_booking_name_value.setGravity(Gravity.LEFT);
            tv_select_date_value.setGravity(Gravity.LEFT);
            tv_start_time_value.setGravity(Gravity.LEFT);
            tv_end_time_value.setGravity(Gravity.LEFT);
            tv_address_value.setGravity(Gravity.LEFT);

        } else {
            tv_booking_name_value.setGravity(Gravity.RIGHT);
            tv_select_date_value.setGravity(Gravity.RIGHT);
            tv_start_time_value.setGravity(Gravity.RIGHT);
            tv_end_time_value.setGravity(Gravity.RIGHT);
            tv_address_value.setGravity(Gravity.RIGHT);

        }


        img_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Preferences.getValue_String(BookingDetailsActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                    if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), BookingDetailsActivity.this)) {
                        String param = "&booking_id=" + AppConstants.booking_id;


                        new CreatePdfApi(param, BookingDetailsActivity.this, new CreatePdfApi.OnResultReceived() {
                            @Override
                            public void onResult(String result)  {

                                Uri path = Uri.parse(AppConstants.pdfpath);
                                Log.e(TAG, "Uri Path : " + path);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(path, "application/pdf");
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        }, true);
                    }
                } else {
                    if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), BookingDetailsActivity.this)) {
                        String param = "&booking_id=" + AppConstants.booking_id;

                        new CreateArabicpdfApi(param, BookingDetailsActivity.this, new CreateArabicpdfApi.OnResultReceived() {
                            @Override
                            public void onResult(String result)  {

                                Uri path = Uri.parse(AppConstants.pdfpath);
                                Log.e(TAG, "Uri Path : " + path);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(path, "application/pdf");
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        }, true);
                    }
                }
            }
        });
        tv_booking_name_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "click...");
                callPermission();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + "966" + phoneno));

                if (ActivityCompat.checkSelfPermission(BookingDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
                Log.e(TAG, "click...");

            }
        });


        if (Preferences.getValue_String(BookingDetailsActivity.this, Preferences.ROLE_ID).equals(AppConstants.EMPLOYEE_ROLE)) {
            tv_booking_name_value.setClickable(true);
            tv_booking_name_value.setEnabled(true);
        } else if (Preferences.getValue_String(BookingDetailsActivity.this, Preferences.ROLE_ID).equals(AppConstants.USER_ROLE)) {
            tv_booking_name_value.setClickable(false);
            tv_booking_name_value.setEnabled(false);
        }


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);


        rel_addorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ORDER_ACTION = AppConstants.NEW_ORDER;
                startActivity(new Intent(BookingDetailsActivity.this, NewOrderActivity.class));
            }
        });
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alertDialog = new AlertDialog.Builder(BookingDetailsActivity.this);
                alertDialog.setTitle(AppConstants.baseWords.get("Confirm_Done"));
                alertDialog.setMessage(AppConstants.baseWords.get("Are_you_sure_you_want_Done_this?"));
                alertDialog.setPositiveButton(AppConstants.baseWords.get("Yes"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), BookingDetailsActivity.this)) {
                            String param = "&booking_id=" + AppConstants.booking_id;
                            param = param + "&done_order=1";

                            new DoneBookingApi(param, BookingDetailsActivity.this, new DoneBookingApi.OnResultReceived() {
                                @Override
                                public void onResult(String result)  {
                                    AppConstants.IshistoryBooking = true;
                                    AppConstants.IscurrentBooking = true;
                                }
                            }, true);
                        }
                    }
                });

                alertDialog.setNegativeButton(AppConstants.baseWords.get("No"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();


            }
        });
    }

    private void setBaseWords() {

        tv_booking_name.setText(AppConstants.baseWords.get("text_lbl_booking_name"));
        tv_select_date.setText(AppConstants.baseWords.get("text_lbl_date"));
        tv_start_time.setText(AppConstants.baseWords.get("text_lbl_start_time"));
        tv_end_time.setText(AppConstants.baseWords.get("text_lbl_end_time"));
        tv_address.setText(AppConstants.baseWords.get("text_lbl_address"));
        tv_orders.setText(AppConstants.baseWords.get("text_lbl_orders"));
        tv_addorder.setText(AppConstants.baseWords.get("text_add_order"));
        tv_done.setText(AppConstants.baseWords.get("text_btn_done"));
        tv_header_title.setText(AppConstants.baseWords.get("text_booking_detail"));

    }

    private void setData() {

        tv_booking_name_value.setText(AppConstants.bookingDetails.getBooking_name());
        Log.e(TAG, AppConstants.bookingDetails.getBooking_name());
        tv_select_date_value.setText(AppConstants.FormatDate(AppConstants.bookingDetails.getBooking_date()));
        tv_start_time_value.setText(AppConstants.bookingDetails.getBooking_start_time());
        tv_end_time_value.setText(AppConstants.bookingDetails.getBooking_end_time());
        tv_address_value.setText(AppConstants.bookingDetails.getAddress());

        String data = AppConstants.bookingDetails.getBooking_name();

        Log.e(TAG, AppConstants.bookingDetails.getBooking_name());
        Log.e(TAG, data);
        String[] items = data.split("-");
        Log.e(TAG, AppConstants.bookingDetails.getBooking_name());
        phoneno = items[2];
        Log.e(TAG, "phone number : " + phoneno);
    }

    private void initView() {
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        tv_done = (TextView) findViewById(R.id.tv_done);
        tv_addorder = (TextView) findViewById(R.id.tv_addorder);
        img_download = (ImageView) findViewById(R.id.img_download);


        img_back = (ImageView) findViewById(R.id.img_back);
        img_option = (ImageView) findViewById(R.id.img_option);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_booking_name = (TextView) findViewById(R.id.tv_booking_name);
        tv_end_time = (TextView) findViewById(R.id.tv_end_time);
        tv_start_time = (TextView) findViewById(R.id.tv_start_time);
        tv_orders = (TextView) findViewById(R.id.tv_orders);
        tv_select_date = (TextView) findViewById(R.id.tv_select_date);

        tv_booking_name_value = (TextView) findViewById(R.id.tv_booking_name_value);
        tv_select_date_value = (TextView) findViewById(R.id.tv_select_date_value);
        tv_start_time_value = (TextView) findViewById(R.id.tv_start_time_value);
        tv_end_time_value = (TextView) findViewById(R.id.tv_end_time_value);
        tv_address_value = (TextView) findViewById(R.id.tv_address_value);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        rel_five = (RelativeLayout) findViewById(R.id.rel_five);
        rel_addorder = (RelativeLayout) findViewById(R.id.rel_addorder);
        rel_view = (RelativeLayout) findViewById(R.id.rel_view);
        rel_map = (RelativeLayout) findViewById(R.id.rel_map);
        btnbooking = (Button) findViewById(R.id.btnbooking);
        btn_cancle = (Button) findViewById(R.id.btn_cancle);
        linearLayoutManager = new LinearLayoutManager(BookingDetailsActivity.this);
        recycler_view.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng sydney = new LatLng(latitude1, longitude1);
        googleMap.addMarker(new MarkerOptions().position(sydney)
                .title(""));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 5.0f));

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                latitude = AppConstants.bookingDetails.getLatitude();
                Log.e(TAG, "Latitude : " + latitude);
                longitude = AppConstants.bookingDetails.getLongitude();
                Log.e(TAG, "Longitude : " + longitude);
                addresss = AppConstants.bookingDetails.getAddress();
                Log.e(TAG, "addresss : " + addresss);
                /*String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
                //String uri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);*/

                String url = "http://maps.google.com/maps?daddr=" + addresss;
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);

                return true;
            }

        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (AppConstants.IsBookingDetails) {
            String param = "&booking_id=" + AppConstants.booking_id;

            new BookingDetailsApi(param, BookingDetailsActivity.this, new BookingDetailsApi.OnResultReceived() {
                @Override
                public void onResult(String result)  {
                    AppConstants.IsBookingDetails = false;
                    latitude1 = Double.parseDouble(AppConstants.bookingDetails.getLatitude());
                    Log.e(TAG, "Latitude : " + latitude1);
                    longitude1 = Double.parseDouble(AppConstants.bookingDetails.getLongitude());
                    Log.e(TAG, "Longitude : " + longitude1);
                    setData();
                    mapFragment.getMapAsync(BookingDetailsActivity.this);

                    if (AppConstants.orderlistArraylist.size() > 0) {
                        recycler_view.setVisibility(View.VISIBLE);
                        rel_view.setVisibility(View.VISIBLE);
                        adapter = new BookingDetailsAdapter(BookingDetailsActivity.this, AppConstants.orderlistArraylist);
                        recycler_view.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        recycler_view.setVisibility(View.GONE);
                        rel_view.setVisibility(View.GONE);
                    }

                }

            }, true);
        } else {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }

        if (Preferences.getValue_String(BookingDetailsActivity.this, Preferences.ROLE_ID).equals(AppConstants.USER_ROLE)) {
            rel_addorder.setVisibility(View.GONE);
            rel_five.setVisibility(View.GONE);
            tv_done.setVisibility(View.GONE);
            img_download.setVisibility(View.GONE);

        } else {

            if (AppConstants.ACTION.equals(AppConstants.HISTORY_DETAILS)) {
                rel_addorder.setVisibility(View.GONE);
                tv_done.setVisibility(View.VISIBLE);
                img_download.setVisibility(View.VISIBLE);
            } else {
                rel_addorder.setVisibility(View.VISIBLE);
                tv_done.setVisibility(View.GONE);
                img_download.setVisibility(View.GONE);
            }

            rel_five.setVisibility(View.VISIBLE);
            btnbooking.setText(AppConstants.baseWords.get("text_btn_done"));
        }


        if (AppConstants.ACTION.equals(AppConstants.NEW_BOOKING)) {
            rel_five.setVisibility(View.VISIBLE);
            rel_addorder.setVisibility(View.VISIBLE);
            btnbooking.setText(AppConstants.baseWords.get("CONFIRM_BOOKING"));
            tv_header_title.setText(AppConstants.baseWords.get("text_lbl_copybookings").toUpperCase());
        } else {


            tv_header_title.setText(AppConstants.baseWords.get("text_booking_detail").toUpperCase());
            rel_five.setVisibility(View.GONE);


        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                Map<String, Integer> perms = new HashMap<>();

                perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);


                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {


                } else {


                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    protected void callPermission() {
        Log.e(TAG, "call");
        if (Build.VERSION.SDK_INT >= 23) {
            Log.e(TAG, "sdk");
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "self permission");
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
                    Log.e(TAG, "should show permission");
                    String message = "You need to grant CALL PHONE permission to send sms";
                    showMessageOkCancle(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= 23) {
                                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                                    }
                                }
                            });
                    return;
                } else {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                    return;
                }
            }
            Log.e(TAG, "sdk");
        }
        Log.e(TAG, "call");
    }

    private void showMessageOkCancle(String message, DialogInterface.OnClickListener okListener) {

        new android.app.AlertDialog.Builder(BookingDetailsActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancle", null)
                .create()
                .show();
    }
}
