package com.Thoopksa;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.Thoopksa.api.EditMobileApi;
import com.Thoopksa.api.LogoutApi;
import com.Thoopksa.api.VerifyOTPApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.Preferences;

public class VerificationActivity extends AppCompatActivity {
    private String TAG = getClass().getSimpleName();
    EditText edt_code;
    Button btnSubmit;
    TextView tv_mobile_verification;
    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        //setContentView(R.layout.activity_verification);
        Log.e(TAG, "VerificationActivity");

        edt_code = (EditText) findViewById(R.id.edt_code);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        logo = (ImageView) findViewById(R.id.logo);
        tv_mobile_verification = (TextView) findViewById(R.id.tv_mobile_verification);
        setIcon();
        setBaseWords();


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, " btnSubmit.setOnClickListener");
                if (edt_code.getText().toString().equals("")) {
                    Log.e(TAG, " edt_code.getText().toString().equals(\"\")");
                    DialogManager.errorDialog(VerificationActivity.this, "Verification Error", AppConstants.baseWords.get("Please_Enter_Verification_Code"));
                } else {
                    verifyOTP();
                }
            }
        });
/*
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, " btnSubmit.setOnClickListener");
                if (edt_code.getText().toString().equals("")) {
                    Log.e(TAG, " edt_code.getText().toString().equals(\"\")");
                    DialogManager.errorDialog(VerificationActivity.this, "Verification Error", AppConstants.baseWords.get("Please_Enter_Verification_Code"));
                } else if (AppConstants.verification_code.equals(edt_code.getText().toString())) {
                    Log.e(TAG, " AppConstants.verification_code.equals(edt_code.getText().toString()");
                    if (AppConstants.ACTION.equals(AppConstants.CHANGE_MOBILE)) {
                        Log.e(TAG, "changeMobile()");
                        changeMobile();
                    } else {
                        Log.e(TAG, " AppConstants.ACTION.equals(AppConstants.LOGIN)");
                        Log.e(TAG, AppConstants.LOGIN);
                        if (AppConstants.check_mobile_status.equals("0")) {
                            Log.e(TAG, "AppConstants.check_mobile_status : " + AppConstants.check_mobile_status);
                            Log.e(TAG, "login()");
                            login();
                        } else {
                            Log.e(TAG, "AppConstants.check_mobile_status : " + AppConstants.check_mobile_status);
                            startActivity(new Intent(VerificationActivity.this, AddprofileActivity.class));
                            finish();
                        }
                    }
                } else {
                    Log.e(TAG, "Error...");
                    DialogManager.errorDialog(VerificationActivity.this, "Verification Error", AppConstants.baseWords.get("Please_Enter_Right_Verification_Code"));
                }
            }
        });
*/

        edt_code.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                }
                return false;
            }
        });
    }
    private void setIcon() {
        if (Preferences.getValue_String(VerificationActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            logo.setImageResource(R.drawable.app_logo_english);
        } else {
            logo.setImageResource(R.drawable.app_logo);
        }
    }

    public void verifyOTP() {
        Log.e(TAG, "verifyOTP(){...}");
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), VerificationActivity.this)) {
            String envelope = "&mobile_number=" + AppConstants.country_code + AppConstants.mobile_number_english;
            Log.e(TAG, "Mobile number : " + AppConstants.mobile_number_english);
            envelope = envelope + "&otp=" + edt_code.getText().toString();
            new VerifyOTPApi(envelope, VerificationActivity.this, new VerifyOTPApi.OnResultReceived() {
                @Override
                public void onResult(String result) {
                    Log.e(TAG, "Result : " + result);
                    if (AppConstants.ACTION.equals(AppConstants.CHANGE_MOBILE)) {
                        changeMobile();
                    } else {
                        Log.e(TAG, AppConstants.LOGIN);
                        if (AppConstants.isRegistered.equals("0")) {
                            Log.e(TAG, "Add Profile()");
                            startActivity(new Intent(VerificationActivity.this, AddprofileActivity.class));
                            finish();
                        } else {
                            Log.e(TAG, "login()");
                            //login();
                            startActivity(new Intent(VerificationActivity.this, HomeActivity.class));
                            finish();
                        }
                    }
                }
            }, true);
        }
    }

    private void setBaseWords() {
        edt_code.setHint(AppConstants.baseWords.get("text_verification_hint_code"));
        btnSubmit.setText(AppConstants.baseWords.get("text_btn_verification"));
        tv_mobile_verification.setText(AppConstants.baseWords.get("text_lbl_mobile_num_verification"));
    }

//    @SuppressLint("HardwareIds")
//    private void login() {
//        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), VerificationActivity.this)) {
//            String envelope = "&phoneno=" + AppConstants.mobile_number_english;
//            envelope = envelope + "&role_id=" + AppConstants.USER_ROLE;
//            envelope = envelope + "&gcm_id=" + FirebaseInstanceId.getInstance().getToken();
//            envelope = envelope + "&device_id=" + Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
//            envelope = envelope + "&type=" + "1";
//            new LoginApi(envelope, VerificationActivity.this, new LoginApi.OnResultReceived() {
//                @Override
//                public void onResult(String result) {
//                    Log.e("Error:   ", result);
//                    Intent home = new Intent(VerificationActivity.this, HomeActivity.class);
//                    startActivity(home);
//                    finish();
//                }
//            }, true);
//        }
//    }

    /*
        public void register() {
            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), VerificationActivity.this)) {
                try {
                    String envelope = "&fullname=" + URLEncoder.encode(AppConstants.code_fullname, AppConstants.encodeType);
                    envelope = envelope + "&email=" + URLEncoder.encode(AppConstants.code_email, AppConstants.encodeType);
                    envelope = envelope + "&phoneno=" + URLEncoder.encode(AppConstants.code_mobile, AppConstants.encodeType);
                    Log.e(TAG, "Mobile number : " + AppConstants.code_mobile);
                    envelope = envelope + "&password=" + URLEncoder.encode(AppConstants.code_password, AppConstants.encodeType);


                    new RegisterApi(envelope, VerificationActivity.this, new RegisterApi.OnResultReceived() {
                        @Override
                        public void OnResult(String result) "" {

                        }
                    }, true);
                } catch (UnsupportedEncodingException e) {
                    Log.e(TAG, "UnsupportedEncodingException: " + e.getMessage());
                }
            }

        }
    */
    private static String arabicToDecimal(String number) {
        char[] chars = new char[number.length()];
        for (int i = 0; i < number.length(); i++) {
            char ch = number.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        Log.e("TAG", new String(chars));
        return new String(chars);
    }

    public void changeMobile() {
        Log.e(TAG, "changeMobile()");
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString
                (R.string.network_msg), VerificationActivity.this)) {
            //String envelope = "&user_id=" + ""/*Preferences.getValue_String(VerificationActivity.this, Preferences.USER_ID)*/;
            String envelope = "&phoneno=" + AppConstants.mobile_number_english;
            new EditMobileApi(envelope, VerificationActivity.this, new
                    EditMobileApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) {
                            logout();
                        }
                    }, true);
        }
    }

    @SuppressLint("HardwareIds")
    private void logout() {
        Log.e(TAG, "logout(){...}");
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), VerificationActivity.this)) {
            //String envelope = "&user_id=" + ""/*Preferences.getValue_String(SettingsActivity.this, Preferences.USER_ID)*/;
            String envelope = "&device_id=" + Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            new LogoutApi(envelope, VerificationActivity.this, new LogoutApi.OnResultReceived() {
                @Override
                public void OnResult(String result) {
                    Intent intent = new Intent(VerificationActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    AppConstants.clearPreferences(VerificationActivity.this);
                }
            }, true);
        }
    }
}

