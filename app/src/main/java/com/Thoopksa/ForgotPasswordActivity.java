package com.Thoopksa;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.Thoopksa.api.ForgotPasswordApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.Preferences;

public class ForgotPasswordActivity extends AppCompatActivity {

    private String TAG = getClass().getSimpleName();
    EditText edt_email;
    Button btnSend;
    TextView tv_enter_email;
    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initView();
        setIcon();
        setBaseWords();

        if (Preferences.getValue_String(ForgotPasswordActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            edt_email.setGravity(Gravity.LEFT);

        } else {
            edt_email.setGravity(Gravity.RIGHT);

        }

        edt_email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    send();
                }
                return false;
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        send();
                    }
                }, 300);
            }
        });


    }

    private void setBaseWords() {
        edt_email.setHint(AppConstants.baseWords.get("text_hint_email"));
        tv_enter_email.setText(AppConstants.baseWords.get("text_lbl_enter_email"));
        btnSend.setText(AppConstants.baseWords.get("text_btn_forgot_password"));

    }

    private void initView() {
        edt_email = (EditText) findViewById(R.id.edt_email);
        btnSend = (Button) findViewById(R.id.btnSend);
        tv_enter_email = (TextView) findViewById(R.id.tv_enter_email);
        logo = (ImageView) findViewById(R.id.logo);
    }
    private void setIcon() {
        if (Preferences.getValue_String(ForgotPasswordActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            logo.setImageResource(R.drawable.app_logo_english);
        } else {
            logo.setImageResource(R.drawable.app_logo);
        }
    }

    private void send() {
        if (isValidate()) {
            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), ForgotPasswordActivity.this))
                ;
            try {
                String envelope = "&email=" + edt_email.getText().toString().trim();

                new ForgotPasswordApi(envelope, ForgotPasswordActivity.this, new ForgotPasswordApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) {

                    }
                }, true);
            } catch (Exception e) {
                Log.e(TAG, "UnsupportedEncodingException: " + e.getMessage());

            }
        }
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";
        if (edt_email.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_email");
            flagValidate = false;
        }

        if (!AppConstants.validateEmail(edt_email.getText().toString())) {
            validateMsg = AppConstants.baseWords.get("Please_enter_valid_email");
            flagValidate = false;
        }

        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(ForgotPasswordActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }

    }

}
