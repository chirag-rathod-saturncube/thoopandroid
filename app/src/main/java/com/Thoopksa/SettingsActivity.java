package com.Thoopksa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Thoopksa.adapter.SelectLanguageAdapter;
import com.Thoopksa.api.ChangePasswordApi;
import com.Thoopksa.api.GenerateOTPApi;
import com.Thoopksa.api.LogoutApi;
import com.Thoopksa.api.ViewProfileApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.Preferences;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.Thoopksa.RegisterActivity.MY_PERMISSIONS_REQUEST_SEND_SMS;

public class SettingsActivity extends AppCompatActivity {
    String TAG = getClass().getSimpleName();
    ImageView img_back;
    TextView tv_header_title;

    // EditText edt_mobile;

    RelativeLayout rel_change_password, rel_change_language, rel_change_mobile, rel_profile, rel_logout;
    TextView tv_change_password, tv_change_language, tv_change_mobile, tv_profile, tv_logout;
    public String phoneNo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        //String envelope = "&user_id=" + ""/*Preferences.getValue_String(SettingsActivity.this, Preferences.USER_ID)*/;
        String envelope = /*"&user_id=" + */""/*Preferences.getValue_String(SettingsActivity.this, Preferences.USER_ID)*/;
        new ViewProfileApi(envelope, SettingsActivity.this, new ViewProfileApi.OnResultReceived() {
            @Override
            public void onResult(String result) {

            }
        }, true);
        initView();
        setBaseWords();

        AppConstants.verification_code = String.valueOf(getRandomNumber(1111, 9999));

        rel_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileDailog(SettingsActivity.this);
            }
        });

        rel_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePasswordDialog(SettingsActivity.this);
            }
        });

        rel_change_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeMobileDialog(SettingsActivity.this);
            }
        });
        rel_change_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeLanguage(SettingsActivity.this);
            }
        });

        rel_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                logout();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                dialog.dismiss();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                builder.setMessage(AppConstants.baseWords.get("Are_you_sure_want_to_logout"))
                        .setPositiveButton(AppConstants.baseWords.get("Yes"), dialogClickListener)
                        .setNegativeButton(AppConstants.baseWords.get("No"), dialogClickListener)
                        .show();
            }
        });
    }

    @SuppressLint("HardwareIds")
    private void logout() {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), SettingsActivity.this)) {
            //String envelope = "&user_id=" + ""/*Preferences.getValue_String(SettingsActivity.this, Preferences.USER_ID)*/;
            String envelope = "&device_id=" + Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            new LogoutApi(envelope, SettingsActivity.this, new LogoutApi.OnResultReceived() {
                @Override
                public void OnResult(String result) {
                    Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    AppConstants.clearPreferences(SettingsActivity.this);
                }
            }, true);
        }
    }


    private void setBaseWords() {

        tv_profile.setText(AppConstants.baseWords.get("Profile"));
        tv_change_password.setText(AppConstants.baseWords.get("Change_Password"));
        tv_change_mobile.setText(AppConstants.baseWords.get("Change_Mobile_Number"));
        tv_change_language.setText(AppConstants.baseWords.get("Change_Language"));
        tv_logout.setText(AppConstants.baseWords.get("Logout"));
        tv_header_title.setText(AppConstants.baseWords.get("text_lbl_settings"));


    }

    private void initView() {
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setVisibility(View.VISIBLE);
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        tv_header_title.setVisibility(View.VISIBLE);
        tv_header_title.setText("SETTINGS");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rel_change_password = (RelativeLayout) findViewById(R.id.rel_change_password);
        tv_change_password = (TextView) findViewById(R.id.tv_change_password);
        rel_change_language = (RelativeLayout) findViewById(R.id.rel_change_language);
        tv_change_language = (TextView) findViewById(R.id.tv_change_language);
        rel_logout = (RelativeLayout) findViewById(R.id.rel_logout);
        tv_logout = (TextView) findViewById(R.id.tv_logout);
        rel_change_mobile = (RelativeLayout) findViewById(R.id.rel_change_mobile);
        tv_change_mobile = (TextView) findViewById(R.id.tv_change_mobile);
        rel_profile = (RelativeLayout) findViewById(R.id.rel_profile);
        tv_profile = (TextView) findViewById(R.id.tv_profile);

        if (Preferences.getValue_String(SettingsActivity.this, Preferences.ROLE_ID).equals(AppConstants.USER_ROLE)) {
            rel_change_password.setVisibility(View.GONE);
        } else {
            rel_change_password.setVisibility(View.VISIBLE);
        }
    }

    private void profileDailog(final Context mContext) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_profile, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        final TextView tv_name = (TextView) dialogView.findViewById(R.id.tv_name);
        final TextView lbl_name = (TextView) dialogView.findViewById(R.id.lbl_name);
        final TextView lbl_email = (TextView) dialogView.findViewById(R.id.lbl_email);
        final TextView lbl_phone = (TextView) dialogView.findViewById(R.id.lbl_phone);
        final TextView tv_dialog_title = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
        final TextView tv_email = (TextView) dialogView.findViewById(R.id.tv_email);
        final TextView tv_phone = (TextView) dialogView.findViewById(R.id.tv_phone);
        final TextView btn_ok = (Button) dialogView.findViewById(R.id.btn_ok);

        if (Preferences.getValue_String(SettingsActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            tv_name.setGravity(Gravity.LEFT);
            tv_email.setGravity(Gravity.LEFT);
            tv_phone.setGravity(Gravity.LEFT);
        } else {
            tv_name.setGravity(Gravity.RIGHT);
            tv_email.setGravity(Gravity.RIGHT);
            tv_phone.setGravity(Gravity.RIGHT);
        }

        lbl_name.setText(AppConstants.baseWords.get("Full_Name") + " :");
        lbl_email.setText(AppConstants.baseWords.get("text_hint_email") + " :");
        lbl_phone.setText(AppConstants.baseWords.get("phone_number") + " :");
        tv_dialog_title.setText(AppConstants.baseWords.get("Profile"));
        btn_ok.setText(AppConstants.baseWords.get("OK"));


        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), SettingsActivity.this)) {
//            /String envelope = "&user_id=" + ""/*Preferences.getValue_String(SettingsActivity.this, Preferences.USER_ID)*/;
            String envelope = /*"&user_id=" + */""/*Preferences.getValue_String(SettingsActivity.this, Preferences.USER_ID)*/;
            new ViewProfileApi(envelope, mContext, new ViewProfileApi.OnResultReceived() {
                @Override
                public void onResult(String result) {
                    tv_name.setText(AppConstants.viewprofile.getFullname());
                    tv_email.setText(AppConstants.viewprofile.getEmail());
                    tv_phone.setText(AppConstants.viewprofile.getPhoneno());
                }
            }, true);
        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    private void changePasswordDialog(final Context mContext) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_change_password, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        final TextView tv_dialog_title = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
        final EditText edt_old_pass = (EditText) dialogView.findViewById(R.id.edt_old_pass);
        final EditText edt_new_pass = (EditText) dialogView.findViewById(R.id.edt_new_pass);
        final EditText edt_confirm_pass = (EditText) dialogView.findViewById(R.id.edt_confirm_pass);

        final TextView tv_save = (TextView) dialogView.findViewById(R.id.tv_save);
        final TextView tv_cancel = (TextView) dialogView.findViewById(R.id.tv_cancel);

        if (Preferences.getValue_String(SettingsActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            edt_old_pass.setGravity(Gravity.LEFT);
            edt_new_pass.setGravity(Gravity.LEFT);
            edt_confirm_pass.setGravity(Gravity.LEFT);
        } else {
            edt_old_pass.setGravity(Gravity.RIGHT);
            edt_new_pass.setGravity(Gravity.RIGHT);
            edt_confirm_pass.setGravity(Gravity.RIGHT);
        }

        tv_dialog_title.setText(AppConstants.baseWords.get("text_btn_changepassword"));
        edt_old_pass.setHint(AppConstants.baseWords.get("Old_Password"));
        edt_new_pass.setHint(AppConstants.baseWords.get("New_Password"));
        edt_confirm_pass.setHint(AppConstants.baseWords.get("Confirm_Password"));
        tv_save.setText(AppConstants.baseWords.get("text_btn_save"));
        tv_cancel.setText(AppConstants.baseWords.get("Cancle"));
        //tv_save.setText(AppConstants.baseWords.get("OK"));

        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean flagValidate = true;
                String validateMsg = "";
                if (edt_old_pass.getText().toString().trim().equals("")) {
                    validateMsg = AppConstants.baseWords.get("Please_enter_old_password");
                    flagValidate = false;
                } else if (edt_new_pass.getText().toString().trim().equals("")) {
                    validateMsg = AppConstants.baseWords.get("Please_enter_new_password");
                    flagValidate = false;
                } else if (edt_confirm_pass.getText().toString().trim().equals("")) {
                    validateMsg = AppConstants.baseWords.get("Please_enter_confirm_password");
                    flagValidate = false;
                } else if (!edt_new_pass.getText().toString().equals(edt_confirm_pass.getText().toString())) {
                    validateMsg = AppConstants.baseWords.get("Not_match_confirm_password");
                    flagValidate = false;
                } else {
                    flagValidate = true;
                }

                if (flagValidate) {
                    b.dismiss();
                    if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), SettingsActivity.this)) {
                        //String envelope = "&user_id=" + ""/*Preferences.getValue_String(SettingsActivity.this, Preferences.USER_ID)*/;
                        String envelope = "&oldPass=" + edt_old_pass.getText().toString();
                        envelope = envelope + "&newPass=" + edt_new_pass.getText().toString();
                        new ChangePasswordApi(envelope, mContext, new ChangePasswordApi.OnResultReceived() {
                            @Override
                            public void OnResult(String result) {
                                Preferences.setValue(SettingsActivity.this, Preferences.USER_ID, "");
                                Intent intent = new Intent(mContext, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }, true);


                    }
                } else {
                    DialogManager.errorDialog(mContext, "Error!", validateMsg);
                }

            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    private int getRandomNumber(int min, int max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }

    private void changeMobileDialog(final Context mContext) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_change_mobile, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog d = dialogBuilder.create();
        d.show();

        final EditText edt_mobile = (EditText) dialogView.findViewById(R.id.edt_mobile);
        final TextView tv_verify = (TextView) dialogView.findViewById(R.id.tv_verify);
        final TextView tv_dialog_title = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
        final TextView tv_cancel = (TextView) dialogView.findViewById(R.id.tv_cancel);
        //final Spinner spn_countrycode = (Spinner) dialogView.findViewById(R.id.spn_countrycode);


       /* String[] country = new String[]{

                "966",
                "91",
                "965",
                "974",
                "971"
        };

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_items, country
        );

        if (Preferences.getValue_String(SettingsActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            spn_countrycode.setGravity(Gravity.LEFT);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_items);
            spn_countrycode.setAdapter(spinnerArrayAdapter);
        } else {
            spn_countrycode.setGravity(Gravity.RIGHT);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_items_right);
            spn_countrycode.setAdapter(spinnerArrayAdapter);
        }*/


        tv_dialog_title.setText(AppConstants.baseWords.get("Change_Mobile_Number"));
        edt_mobile.setHint(AppConstants.baseWords.get("text_hint_mobile"));

        tv_verify.setText(AppConstants.baseWords.get("text_btn_verify"));
        tv_cancel.setText(AppConstants.baseWords.get("Cancle"));

        tv_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), SettingsActivity.this)) {
                    if (isValidate(edt_mobile)) {
                        String envelope = "&mobile_number=" + AppConstants.country_code + arabicToDecimal(edt_mobile.getText().toString());
                        Log.e(TAG, "Mobile number : " + edt_mobile.getText().toString());
                        AppConstants.mobile_number_arabic = edt_mobile.getText().toString();
                        AppConstants.mobile_number_english = AppConstants.arabicToDecimal(edt_mobile.getText().toString());
                        if (Preferences.getValue_String(SettingsActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
                            envelope = envelope + "&language_id=" + AppConstants.lang_code_english;
                        } else {
                            envelope = envelope + "&language_id=" + AppConstants.lang_code_arabic;
                        }
                        new GenerateOTPApi(envelope, SettingsActivity.this, new GenerateOTPApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) {
                                AppConstants.ACTION = AppConstants.CHANGE_MOBILE;
                                Log.e(TAG, "Result : " + result);
                            }
                        }, true);
                    }
                }
            }
        });

        /*
        tv_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobile = AppConstants.viewprofile.getPhoneno();
                send();
                AppConstants.mobile_code = arabicToDecimal(edt_mobile.getText().toString());
                Log.e(TAG, "phone number1 : " + AppConstants.mobile_code);
                Log.e(TAG, "phone number2 : " + AppConstants.viewprofile.getPhoneno());
                Log.e(TAG, "phone number3 : " + Preferences.getValue_String(SettingsActivity.this, Preferences.PHONE_NO));
                if (AppConstants.viewprofile.getPhoneno().equals(edt_mobile.getText().toString())) {
                    DialogManager.errorDialog(SettingsActivity.this, "Error", AppConstants.baseWords.get("Mobile_Already_Registered"));
                } else if (edt_mobile.getText().toString().equals("")) {
                    DialogManager.errorDialog(SettingsActivity.this, "Error", AppConstants.baseWords.get("Please_enter_mobile_number"));
                } else if (!(edt_mobile.getText().toString().length() == 9)) {
                    DialogManager.errorDialog(SettingsActivity.this, "Error", ("Please enter 9 digit mobile number"));
                } else if (!edt_mobile.getText().toString().startsWith("5")) {
                    DialogManager.errorDialog(SettingsActivity.this, "Error", ("mobile number start with 5"));
                } else {
                    // check permission is given
                    if (ContextCompat.checkSelfPermission(SettingsActivity.this, android.Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                        // request permission (see result in onRequestPermissionsResult() method)
                        ActivityCompat.requestPermissions(SettingsActivity.this,
                                new String[]{android.Manifest.permission.SEND_SMS},
                                MY_PERMISSIONS_REQUEST_SEND_SMS);
                    } else {
                        send();
                        // Log.e(TAG, convert16to32(String.valueOf(getRandomNumber(1111, 9999))));
                        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), SettingsActivity.this)) {
                            String envelope = "&mobile=966553437652";
                            envelope = envelope + "&password=thoop200";
                            envelope = envelope + "&numbers=" + "966" + edt_mobile.getText().toString();
                            envelope = envelope + "&sender=Thoop";


                            envelope = envelope + "&msg=" + getString(R.string.Dear)
                                    + getString(R.string.To) + getString(R.string.change) + getString(R.string.Phone) + getString(R.string.number) + getString(R.string.semicolon)
                                    + getString(R.string.please) + getString(R.string.enter) + getString(R.string.the) + getString(R.string.number) + getString(R.string.in) + getString(R.string.the) + getString(R.string.Application)
                                    + getString(R.string.verification) + getString(R.string.number) + Unicode(AppConstants.verification_code);

                            */
        /*next line : 00000085 *//*
         */
        /*space : 00000020 *//*


                            envelope = envelope + "&timeSend=0";
                            envelope = envelope + "&dateSend=0";
                            envelope = envelope + "&applicationType=68";
                            envelope = envelope + "&domainName=demo.Thoopksa.com";
                            envelope = envelope + "&msgId=";

                            new SMSApi(envelope, SettingsActivity.this, new SMSApi.OnResultReceived() {
                                @Override
                                public void onResult(String result)  {
                                    AppConstants.ACTION = AppConstants.CHANGE_MOBILE;
                                }
                            }, true);
                        }
                        d.dismiss();
                    }
                }
            }
        });
*/
        tv_cancel.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
    }

    private boolean isValidate(EditText edt_mobile) {
        boolean flagValidate = true;
        String validateMsg = "";

        if (edt_mobile.getText().toString().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_mobile_number");
            flagValidate = false;
        } else if (!edt_mobile.getText().toString().startsWith("5")) {
            validateMsg = AppConstants.baseWords.get("Phone_number__should_start_with_5");
            flagValidate = false;
        } else if (!(edt_mobile.getText().toString().length() == 9)) {
            validateMsg = AppConstants.baseWords.get("Please_enter_valid_9_digit_phone_number");
            flagValidate = false;
        }

        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(SettingsActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }

    public String Unicode(String s) {
        String value = "";
        for (int i = 0; i < s.length(); i++) {

            if (String.valueOf(s.charAt(i)).equals("1")) {
                value = value + "00000031";
            } else if (String.valueOf(s.charAt(i)).equals("2")) {
                value = value + "00000032";
            } else if (String.valueOf(s.charAt(i)).equals("3")) {
                value = value + "00000033";
            } else if (String.valueOf(s.charAt(i)).equals("4")) {
                value = value + "00000034";
            } else if (String.valueOf(s.charAt(i)).equals("5")) {
                value = value + "00000035";
            } else if (String.valueOf(s.charAt(i)).equals("6")) {
                value = value + "00000036";
            } else if (String.valueOf(s.charAt(i)).equals("7")) {
                value = value + "00000037";
            } else if (String.valueOf(s.charAt(i)).equals("8")) {
                value = value + "00000038";
            } else if (String.valueOf(s.charAt(i)).equals("9")) {
                value = value + "00000039";
            } else if (String.valueOf(s.charAt(i)).equals("0")) {
                value = value + "00000030";
            }
        }
        return value;
    }

    private static String arabicToDecimal(String number) {
        char[] chars = new char[number.length()];
        for (int i = 0; i < number.length(); i++) {
            char ch = number.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        Log.e("TAG", new String(chars));
        return new String(chars);
    }

    private void changeLanguage(final Context mContext) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_language, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog dialog = dialogBuilder.create();
        dialog.show();


        final TextView tv_dialog_title = (TextView) dialogView.findViewById(R.id.tv_dialog_title);

        tv_dialog_title.setText(AppConstants.baseWords.get("text_lbl_change_language"));

        final RecyclerView recycler_view = (RecyclerView) dialogView.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(mContext);
        mLayoutManager.setSmoothScrollbarEnabled(true);
        recycler_view.setLayoutManager(mLayoutManager);

        SelectLanguageAdapter selectLanguageAdapter;
        if (AppConstants.languageArrayList.size() > 0) {
            selectLanguageAdapter = new SelectLanguageAdapter(mContext, AppConstants.languageArrayList, dialog);
            recycler_view.setAdapter(selectLanguageAdapter);
            selectLanguageAdapter.notifyDataSetChanged();
        }

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    private void setBasedWords() {

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();

                perms.put(android.Manifest.permission.SEND_SMS, PackageManager.PERMISSION_GRANTED);


                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {


                } else {


                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    protected void send() {
        Log.e(TAG, "send");
        if (Build.VERSION.SDK_INT >= 23) {
            Log.e(TAG, "sdk");
            if (checkSelfPermission(android.Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "self permission");
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.SEND_SMS)) {
                    Log.e(TAG, "should show permission");
                    String message = "You need to grant SEND SMS permission to send sms";
                    showMessageOkCancle(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= 23) {
                                        requestPermissions(new String[]{android.Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);
                                    }
                                }
                            });
                    return;
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);
                    return;
                }
            }
            Log.e(TAG, "sdk");
        }
        Log.e(TAG, "send");
    }

    private void showMessageOkCancle(String message, DialogInterface.OnClickListener okListener) {

        new android.app.AlertDialog.Builder(SettingsActivity.this)
                .setMessage(message)
                .setPositiveButton(AppConstants.baseWords.get("OK"), okListener)
                .setNegativeButton(AppConstants.baseWords.get("Cancle"), null)
                .create()
                .show();
    }
}