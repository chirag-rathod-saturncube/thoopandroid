package com.Thoopksa;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.Thoopksa.api.AddProfileApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.Preferences;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.Locale;

public class AddprofileActivity extends AppCompatActivity {

    TextView tv_header_title;
    ImageView img_back;
    EditText edt_name, edt_email, edt_phone;
    Button btn_save;
    public String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addprofile);
        initView();
        setBaseWords();
        if (Preferences.getValue_String(AddprofileActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            edt_email.setGravity(Gravity.LEFT);
            edt_name.setGravity(Gravity.LEFT);
            edt_phone.setGravity(Gravity.LEFT);

        } else {
            edt_email.setGravity(Gravity.RIGHT);
            edt_name.setGravity(Gravity.RIGHT);
            edt_phone.setGravity(Gravity.RIGHT);
        }
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProfile();
            }
        });
    }

    private void addProfile() {
        if (isValidate()) {
            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), AddprofileActivity.this)) {
                String envelope = null;
                try {
                    envelope = "&fullname=" + URLEncoder.encode(edt_name.getText().toString(), AppConstants.encodeType);
                    envelope = envelope + "&email=" + edt_email.getText().toString();
                    NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
                    Log.e(TAG, "Number : " + edt_phone.getText().toString());
                    // Log.e(TAG, "Formated Number : " + nf.format(edt_phone.getText().toString()));
                    Log.e(TAG, "Formated Number : " + AppConstants.arabicToDecimal(edt_phone.getText().toString()));
                    //envelope = envelope + "&phoneno=" +/* nf.format(*/edt_phone.getText().toString()/*)*/;
                    envelope = envelope + "&phoneno=" + AppConstants.arabicToDecimal(edt_phone.getText().toString());
                    new AddProfileApi(envelope, AddprofileActivity.this, new AddProfileApi.OnResultReceived() {
                        @Override
                        public void OnResult(String result) {
                            //login();
                            startActivity(new Intent(AddprofileActivity.this, HomeActivity.class));
                            finish();
                        }
                    }, true);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setBaseWords() {
        edt_name.setHint(AppConstants.baseWords.get("text_hint_full_name"));
        //edt_password.setHint(AppConstants.baseWords.get("text_hint_password"));
        edt_email.setHint(AppConstants.baseWords.get("text_hint_email"));
        //tv_register.setText(AppConstants.baseWords.get("text_lbl_register"));

        //tv_forgot.setText(AppConstants.baseWords.get("text_lbl_forgot_password"));


    }

    private void initView() {
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        tv_header_title.setText("Add Profile");
        img_back = (ImageView) findViewById(R.id.img_back);
        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_phone = (EditText) findViewById(R.id.edt_phone);
        edt_phone.setText(AppConstants.mobile_number_arabic);
        btn_save = (Button) findViewById(R.id.btn_save);
    }

//    @SuppressLint("HardwareIds")
//    private void login() {
//        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), AddprofileActivity.this)) {
//            String envelope = "&phoneno=" + AppConstants.mobile_number_english;
//            envelope = envelope + "&role_id=" + AppConstants.USER_ROLE;
//            envelope = envelope + "&gcm_id=" + FirebaseInstanceId.getInstance().getToken();
//            envelope = envelope + "&device_id=" + Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
//            envelope = envelope + "&type=" + "1";
//            new LoginApi(envelope, AddprofileActivity.this, new LoginApi.OnResultReceived() {
//                @Override
//                public void onResult(String result) {
//                    Log.e("Error:   ", result);
//                    Intent home = new Intent(AddprofileActivity.this, HomeActivity.class);
//                    startActivity(home);
//                    finish();
//                }
//            }, true);
//        }
//    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";
        if (edt_name.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_fullname");
            flagValidate = false;
        } else if (!edt_email.getText().toString().equals("")) {
            if (!AppConstants.validateEmail(edt_email.getText().toString())) {
                validateMsg = AppConstants.baseWords.get("Please_enter_valid_email");
                flagValidate = false;
            }
        } else if (edt_phone.getText().toString().trim().equals("")) {
            validateMsg = AppConstants.baseWords.get("Please_enter_mobile_number");
            flagValidate = false;
        } else if (!edt_phone.getText().toString().startsWith("5")) {
            validateMsg = AppConstants.baseWords.get("Phone_number__should_start_with_5");
            flagValidate = false;
        } else if (!(edt_phone.getText().toString().length() == 9)) {
            validateMsg = AppConstants.baseWords.get("Please_enter_valid_9_digit_phone_number");
            flagValidate = false;
        }
        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(AddprofileActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }


}
