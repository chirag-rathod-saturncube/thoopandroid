package com.Thoopksa;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.Thoopksa.api.EmployeeLoginApi;
import com.Thoopksa.common.AppConstants;
import com.Thoopksa.common.DialogManager;
import com.Thoopksa.common.Preferences;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class EmployeeLoginActivity extends AppCompatActivity {
    private String TAG = getClass().getSimpleName();
    EditText edt_email, edt_password;
    Button btnLogin;
    TextView tv_employee_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_login);
        initView();
        setBaseWords();
        if (Preferences.getValue_String(EmployeeLoginActivity.this, Preferences.LANGUAGE_CODE).equals("txt_eng")) {
            edt_email.setGravity(Gravity.LEFT);
            edt_password.setGravity(Gravity.LEFT);
        } else {
            edt_password.setGravity(Gravity.RIGHT);
            edt_email.setGravity(Gravity.RIGHT);
        }
        edt_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    login();
                }
                return false;
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        login();
                    }
                }, 300);
            }
        });
    }

    private void setBaseWords() {
        edt_email.setHint(AppConstants.baseWords.get("text_hint_email"));
        edt_password.setHint(AppConstants.baseWords.get("text_hint_password"));
        btnLogin.setText(AppConstants.baseWords.get("text_btn_login"));
        tv_employee_login.setText(AppConstants.baseWords.get("text_lbl_employee_login"));
    }

    private void login() {
        if (isValidate()) {
            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), EmployeeLoginActivity.this)) {
                try {
                    String envelope = "&email=" + edt_email.getText().toString().trim();
                    envelope = envelope + "&password=" + URLEncoder.encode(edt_password.getText().toString().trim(), AppConstants.encodeType);
                    envelope = envelope + "&role_id=" + AppConstants.EMPLOYEE_ROLE;
                    envelope = envelope + "&gcm_id=" + FirebaseInstanceId.getInstance().getToken();
                    envelope = envelope + "&device_id=" + Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
                    envelope = envelope + "&type=" + "1";

                    new EmployeeLoginApi(envelope, EmployeeLoginActivity.this, new EmployeeLoginApi.OnResultReceived() {
                        @Override
                        public void onResult(String result)  {
                            Log.e("Error : ", result);
                            Intent home = new Intent(EmployeeLoginActivity.this, HomeActivity.class);
                            home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(home);
                            finish();
                        }
                    }, true);
                } catch (UnsupportedEncodingException e) {
                    Log.e(TAG, "UnsupportedEncodingException: " + e.getMessage());
                }
            }
        }
    }

    private boolean isValidate() {
        boolean flagvalidate = true;
        String validatemsg = "";

        if (edt_email.getText().toString().trim().equals("")) {
            validatemsg = AppConstants.baseWords.get("Please_enter_email");
            flagvalidate = false;
        } else if (!AppConstants.validateEmail(edt_email.getText().toString().trim())) {
            validatemsg = AppConstants.baseWords.get("Please_enter_valid_email");
            flagvalidate = false;
        } else if (edt_password.getText().toString().trim().equals("")) {
            validatemsg = AppConstants.baseWords.get("Please_enter_password");
            flagvalidate = false;
        } else {
            flagvalidate = true;
        }
        if (flagvalidate) {
            return true;
        } else {
            DialogManager.errorDialog(EmployeeLoginActivity.this, "Error!", validatemsg);
            return false;
        }
    }

    private void initView() {
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tv_employee_login = (TextView) findViewById(R.id.tv_employee_login);
    }


}
